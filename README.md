# README for Field Day

Welcome to Field Day!

Field Day is an Android application for interfacing between field researchers, DIY Arduino sensor platforms, and remote databases. It is developed by the Earlham Computer Science Department students and faculty, for the Earlham College EPIC field science programs.

# Getting the code and building Field Day

Getting and running Field Day requires Android Studio. It has most recently been built in Android Studio 3.5.1. Import it as a Git project through the URL of the source repo.

On your phone or tablet: turn on developer mode, and when prompted confirm that you trust your computer. 

# Running the app

A common use of Field Day goes like this:

1. Open the app.
2. Connect to your database. (Note: This means your server-side database configuration should be complete. If it is not, do that before attempting the connection.)
3. Set up the relevant trip data.
4. Connect to your BlueTooth device. (Note: This means your Arduino platforms should be running the most up-to-date version of the platforms software.)
4. Calibrate if necessary.
5. Configure the relevant settings, e.g. if you intend to stream, select the sampling interval.
6. Choose stream or sample.
7. (If streaming) Turn off the screen and put Field Day in your pocket. Keep the platform on your person as you walk around. Press the stream button again to toggle it off when done.
8. At the end of the day, upload the new data to the database you've configured.

# Developer guidelines

If you're making changes, create a branch, make commits there, push the new branch to this GitLab project, and submit a merge request when it's complete.
