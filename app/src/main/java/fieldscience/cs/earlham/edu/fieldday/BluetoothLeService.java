package fieldscience.cs.earlham.edu.fieldday;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.UUID;

public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    private BluetoothManager mBluetoothManager;
    private BluetoothSensor btSensor;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA = "EXTRA_DATA";
    public final static String LIST_OF_SENSORS = "LIST_OF_SENSORS";
    public final static String SENSOR_VALUES = "SENSOR_VALUES";

    public static String current_request = "";
    public static boolean isBean = false;

    private final static UUID UUID_BLE_SENSOR_VALUE = UUID
            .fromString(GattAttributes.BLE_SENSOR_VALUE_RX);
    private final static UUID UUID_BLE_SENSOR_REQUEST = UUID
            .fromString(GattAttributes.BLE_SENSOR_REQUEST_TX);
    private final static UUID UUID_BLE_SENSOR_SERVICE = UUID
            .fromString(GattAttributes.BLE_SENSOR_SERVICE);
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    private final BluetoothSensor.Listener listener = new BluetoothSensor.Listener() {
        @Override
        public void onCharacteristicRead(UUID characteristicUUID, String data) {
            broadcastUpdate(data);
        }

        @Override
        public void onServiceInformation() {
            for (BluetoothGattService s : btSensor.getServices()){
                 if (s.getUuid().equals(UUID_BLE_SENSOR_SERVICE)) {
                     BluetoothGattService gattService = btSensor.getService(UUID_BLE_SENSOR_SERVICE);
                    //btSensor.setPrimaryService(gattService);
                    btSensor.setCharacteristics(
                            gattService.getCharacteristic(UUID_BLE_SENSOR_REQUEST),
                            gattService.getCharacteristic(UUID_BLE_SENSOR_VALUE));
                    btSensor.setToNotify(gattService.getCharacteristic(UUID_BLE_SENSOR_VALUE), true);
                    break;
                }
            }
            broadcastUpdate();
        }
    };


    private void broadcastUpdate(String data) {
        final Intent intent = new Intent(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intent.putExtra(EXTRA_DATA, data);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(){
        final Intent intent = new Intent(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    public boolean initialize() {
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        BluetoothAdapter mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }
        return true;
    }

    public void connect(BluetoothSensor bluetoothSensor) {
        btSensor = bluetoothSensor;
        int mConnectionState = STATE_CONNECTING;
        btSensor.setListener(listener);
        btSensor.connect(this);
    }

    public void disconnect() {
        btSensor.disconnect();
    }

    public void close() {
        btSensor.disconnect();
    }

    public void readMessage() {
        btSensor.readMessage();
    }

    public void writeMessage(String message, String request) {
        byte b = 0x00;
        byte[] tmp = message.getBytes();
        byte[] tx = new byte[tmp.length + 1];

        tx[0] = b;
        System.arraycopy(tmp, 0, tx, 1, tmp.length + 1 - 1);
        current_request = request;
        btSensor.writeMessage(tx);
    }
}