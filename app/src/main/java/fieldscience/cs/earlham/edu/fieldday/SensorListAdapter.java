package fieldscience.cs.earlham.edu.fieldday;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

class SensorListAdapter extends ArrayAdapter<aSensor> {

    private final Context context;

    public SensorListAdapter(Context c, ArrayList<aSensor> sensorArrayList) {
        super(c, R.layout.fragment_sensorlist, sensorArrayList);
        context = c;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolderItem holder;
        aSensor sensor = getItem(position);
        Resources res = context.getResources();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.fragment_sensorlist, parent, false);

            holder = new ViewHolderItem();
            holder.sensorName = convertView.findViewById(R.id.sensor);
            holder.sensorValue = convertView.findViewById(R.id.sensorValue);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolderItem) convertView.getTag();
        }

        holder.sensorName.setText(String.format(res.getString(R.string.built_in_sensor),
                                    Objects.requireNonNull(sensor).getName()));
        if ((sensor.getLastValues() == null) || (sensor.getLastValueString().equals(""))) {
            holder.sensorValue.setText("");
        } else {
            holder.sensorValue.setText(sensor.getLastValueString());
        }

        return convertView;
    }

    static class ViewHolderItem {
        TextView sensorName;
        TextView sensorValue;


        ViewHolderItem() {
            // empty constructor
        }
    }


}
