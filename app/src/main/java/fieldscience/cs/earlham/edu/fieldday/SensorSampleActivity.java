package fieldscience.cs.earlham.edu.fieldday;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import androidx.preference.PreferenceManager;

import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SensorSampleActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private static TextView longAndLat;
    private static TextView satellites;
    private static TextView accuracy;
    private static TextView elevation;
    private static TextView batteryStatus;
    private static Resources res;
    private static String tripName;
    private static String site;
    private static String sector;
    private static String spot;
    private static String sensor;
    private static String pictureName;
    private ReadingsDatabase db;
    private RadioGroup logIntervalButton;
    private long logInterval = 5000;
    private ArrayList<aSensor> listSensors;
    private ArrayList<aSensor> sensors;
    private Boolean streaming = false;
    private Drawable streamOn;
    private Drawable streamOff;
    private Handler buttonHandler;
    private Runnable buttonRunner;
    private ImageButton streamButton;
    private ImageButton sampleButton;
    public static final String DEVICE = "BluetoothSensor Instance";
    public static final String WHICH_SENSOR = "Which Sensor";
    public static final String REMOTE_DB = "Remote DB Connected";
    private Button calibrateButton;
    private BluetoothSensor bluetoothSensor;
    private SensorSampleService sampleService;
    private int streamStatusId = 0;
    private Context context;
    private ArrayAdapter<aSensor> adapter;
    private ProgressDialog progress;
    private File baseDir;
    private String tmp;
    private Uri pictureUri;

    private static final String directory = ReadingsDatabase.directory;

    private final ServiceConnection sampleServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            sampleService = ((SensorSampleService.LocalBinder) service).getService();
            sampleService.connectSensor(sensor, bluetoothSensor);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    private final BroadcastReceiver sampleServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SensorSampleService.ACTION_CONNECTED.equals(intent.getAction())) {
                enableButtons();
                sensors = intent.getParcelableArrayListExtra(SensorSampleService.SENSOR_LIST);
                adapter.clear();
                adapter.addAll(sensors);
                adapter.notifyDataSetChanged();
                progress.dismiss();
                calibrateButton.setVisibility(View.VISIBLE);
                Log.d("BATT_TEST_CONN", adapter.toString());
                batteryStatus.setText("");
            } else if (SensorSampleService.ACTION_UPDATED_LIST.equals(intent.getAction())) {
                sensors = intent.getParcelableArrayListExtra(SensorSampleService.SENSOR_LIST);
                adapter.clear();
                adapter.addAll(sensors);
                adapter.notifyDataSetChanged();
                Log.d("BATT_TEST_UP", adapter.toString());
            } else if (SensorSampleService.ACTION_DISCONNECTED.equals(intent.getAction())) {
                buttonHandler.removeMessages(0);
                buttonHandler.removeCallbacksAndMessages(buttonRunner);
                streamStatusId = 0;
                changeStreamButton();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensorsample);
        initializeVariables();

        Intent sensorServiceIntent = new Intent(this, SensorSampleService.class);
        bindService(sensorServiceIntent, sampleServiceConnection, Context.BIND_AUTO_CREATE);
        registerReceiver(sampleServiceReceiver, makeSensorUpdateFilter());

        sensor = getIntent().getStringExtra(WHICH_SENSOR);
        if (Objects.requireNonNull(sensor).equals("bluetooth")) {
            bluetoothSensor = getIntent().getParcelableExtra(DEVICE);
            streamButton.setImageDrawable(res.getDrawable(R.drawable.stream_unavailable, null));
            sampleButton.setImageDrawable(res.getDrawable(R.drawable.sample_unavailable, null));
            sampleButton.setClickable(false);
            streamButton.setClickable(false);
            Cursor c = db.query("fieldday_platform", "platformName=?", new String[]{bluetoothSensor.getDeviceName()});
            c.moveToFirst();
            if (c.getString(c.getColumnIndex("platformName")).equals("Ambiance")) {
                Boolean calibrationAvailable = true;
            }
            progress = new ProgressDialog(this);
            progress.setMessage("Please wait. Connecting to device.");
            progress.setCancelable(false);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            RadioButton three = findViewById(R.id.threeSeconds);
            three.setVisibility(View.INVISIBLE);
        } else {
            RadioButton one = findViewById(R.id.oneSecond);
            one.setVisibility(View.VISIBLE);
        }

        ListView mSensors = findViewById(R.id.sensor_and_value_list);
        adapter = new SensorListAdapter(this, listSensors);
        mSensors.setAdapter(adapter);
        Intent i = new Intent(context, SensorSampleService.class);

        logIntervalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeInterval(v);
            }
        });

        streamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (site == null || sector == null || site.equals("") || sector.equals("")) {
                    Toast t = Toast.makeText(SensorSampleActivity.this, "You must pick a site and sector to stream.", Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
                else if (!streaming) {
                    streaming = true;
                    streamStatusId = 1;

                    // This starts the handler that will change the streaming button so there's an
                    // indication that Field Day is actually streaming and writing to the database.
                    buttonHandler.postDelayed(buttonRunner, 350);

                    // Start the SensorSampleService supplying the type of sensor connected --
                    // bluetooth or built-in and the logging interval to use.
                    Log.d("STREAMSITESECTOR","starting");
                    Intent i = new Intent(context, SensorSampleService.class);
                    i.putExtra(SensorSampleService.LOG_INTERVAL, logInterval);
                    i.putExtra(SensorSampleService.SAMPLE_ONCE, false);
                    i.putExtra(SensorSampleService.TRIP_NAME, tripName);
                    i.putExtra(SensorSampleService.SITE, site);
                    i.putExtra(SensorSampleService.SECTOR, sector);
                    startService(i);
                } else {
                    // Stop the SensorSampleService from writing to the database. Change the button
                    // for streaming to be off.
                    streamButton.setImageDrawable(streamOff);
                    streaming = false;
                    sampleService.stopSampling();
                    buttonHandler.removeMessages(0);
                    buttonHandler.removeCallbacksAndMessages(buttonRunner);
                    Intent i = new Intent(context, SensorSampleService.class);
                    stopService(i);
                }
            }
        });

        sampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (streaming) {
                    Toast t = Toast.makeText(SensorSampleActivity.this, "Turn off streaming before trying to sample", Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (spot.equals("")) {
                    Toast t = Toast.makeText(SensorSampleActivity.this, "You must pick a spot number for sample.", Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (!db.addSpot(tripName, site, sector, spot)) {
                    Dialog d = new androidx.appcompat.app.AlertDialog.Builder(SensorSampleActivity.this)
                            .setTitle("Are you sure?")
                            .setMessage("There's already a spot with this trip, site, and sector. Make sure you are at the same geolocation that is " +
                                    "associated with this spot.")
                            .setNegativeButton("Change Spot", null)
                            .setPositiveButton("I'm sure.", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(context, SensorSampleService.class);
                                    i.putExtra(SensorSampleService.LOG_INTERVAL, logInterval);
                                    i.putExtra(SensorSampleService.SAMPLE_ONCE, true);
                                    i.putExtra(SensorSampleService.TRIP_NAME, tripName);
                                    i.putExtra(SensorSampleService.SITE, site);
                                    i.putExtra(SensorSampleService.SECTOR, sector);
                                    i.putExtra(SensorSampleService.SPOT, spot);
                                    startService(i);
                                    Log.d("SensorSampleActivity", "Sample once: " + tripName + ", " + site + "," + sector + ", " + spot + ".");
                                }
                            }).create();
                    d.show();
                } else {
                    Intent i = new Intent(context, SensorSampleService.class);

                    Log.d("SensorSampleActivity", "Sample once spot added: " + tripName + ", " + site + "," + sector + ", " + spot + ".");
                    i.putExtra(SensorSampleService.LOG_INTERVAL, logInterval);
                    i.putExtra(SensorSampleService.SAMPLE_ONCE, true);
                    i.putExtra(SensorSampleService.TRIP_NAME, tripName);
                    i.putExtra(SensorSampleService.SITE, site);
                    i.putExtra(SensorSampleService.SECTOR, sector);
                    i.putExtra(SensorSampleService.SPOT, spot);
                    startService(i);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SensorSampleActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SensorSampleActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(locationListener);
        if (sampleService != null) {
            sampleService.stopSelf();
        }
        unregisterReceiver(sampleServiceReceiver);
        unbindService(sampleServiceConnection);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.alert_picture_name, null);
        final EditText name = dialog.findViewById(R.id.pictureName);

        if (resultCode != Activity.RESULT_CANCELED) {
            Dialog d = new androidx.appcompat.app.AlertDialog.Builder(SensorSampleActivity.this)
                    .setTitle("Picture Name")
                    .setView(dialog)
                    .setNegativeButton("Discard Picture", null)
                    .setPositiveButton("Save Picture", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //sampleService.setPictureName(name.getText().toString());
                            pictureName = name.getText().toString();
                            try {
                                dialog.dismiss();
                                new saveImage().execute(android.provider.MediaStore.Images.Media.getBitmap(
                                        context.getContentResolver(), pictureUri));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .show();
        }

    }

    private void enableButtons() {
        streamButton.setClickable(true);
        sampleButton.setClickable(true);
        streamButton.setImageDrawable(res.getDrawable(R.drawable.stream_button, null));
        sampleButton.setImageDrawable(res.getDrawable(R.drawable.sample_button, null));
    }

    private static IntentFilter makeSensorUpdateFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SensorSampleService.ACTION_UPDATED_LIST);
        intentFilter.addAction(SensorSampleService.ACTION_CONNECTED);
        intentFilter.addAction(SensorSampleService.ACTION_DISCONNECTED);
        return intentFilter;
    }

    class myLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            setLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void changeInterval(View v) {
        switch (v.getId()) {
            case R.id.oneSecond:
                logInterval = 1000;
                break;
            case R.id.threeSeconds:
                logInterval = 1000 * 3;
                break;
            case R.id.fiveSeconds:
                logInterval = 1000 * 5;
                break;
            case R.id.tenSeconds:
                logInterval = 1000 * 10;
                break;
            case R.id.thirtySeconds:
                logInterval = 1000 * 30;
                break;
            case R.id.sixtySeconds:
                logInterval = 1000 * 60;
                break;
        }
        sampleService.onLoggingTimeChanged(logInterval);
        Log.d("Changing Interval", "New Interval Time: " + logInterval);
    }

    private void changeStreamButton() {
        switch (streamStatusId) {
            case 0:
                streamButton.setImageDrawable(streamOff);
                streamStatusId = 1;
                break;
            case 1:
                streamButton.setImageDrawable(streamOn);
                streamStatusId = 0;
                break;
        }
    }

    private void setLocation(Location location) {
        if (location != null) {
            longAndLat.setText(String.format(res.getString(R.string.latlong), location.getLatitude(), location.getLongitude()));
            accuracy.setText(String.format(res.getString(R.string.accuracy), location.getAccuracy()));
            satellites.setText(String.format(res.getString(R.string.satellite_count), location.getExtras().getInt("satellites")));
            elevation.setText(String.format(res.getString(R.string.elevation), location.getAltitude()));
        }
    }

    private void initializeVariables() {
        res = getResources();
        longAndLat = findViewById(R.id.longandlat);
        satellites = findViewById(R.id.satellites);
        accuracy = findViewById(R.id.accuracy);
        elevation = findViewById(R.id.elevation);
        Spinner siteSpinner = findViewById(R.id.site);
        Spinner sectorSpinner = findViewById(R.id.sector);
        EditText spotET = findViewById(R.id.spotNumber);
        batteryStatus = findViewById(R.id.battery_status);
        streamButton = findViewById(R.id.streamButton);
        sampleButton = findViewById(R.id.sampleButton);
        streamOn = res.getDrawable(R.drawable.stream_pushed, null);
        streamOff = res.getDrawable(R.drawable.stream_button, null);
        Button pictureButton = findViewById(R.id.addPictureButton);
        Button notesButton = findViewById(R.id.addNotesButton);
        calibrateButton = findViewById(R.id.calibrateSensorButton);

        spot = "";
        pictureName = "";
        baseDir = new File(directory);
        if (!baseDir.exists()) {
            baseDir.mkdir();
        }

        logIntervalButton = findViewById(R.id.interval);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new myLocationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SensorSampleActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        setLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, locationListener);

        db = ReadingsDatabase.getInstance(getApplicationContext());

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        tripName = settings.getString("trip", "");
        Log.d("SensorSample", "tripName: " + tripName);
        TextView trip_name = findViewById(R.id.tripNameText);
        trip_name.setText(String.format("Trip Name: %s", tripName));

        context = this;
        listSensors = new ArrayList<>();
        boolean remote_db = getIntent().getBooleanExtra(REMOTE_DB, false);

        buttonHandler = new Handler();
        buttonRunner = new Runnable() {
            public void run() {
                changeStreamButton();
                buttonHandler.postDelayed(this, logInterval);
            }
        };

        List<Pair<String, String>> siteList = new ArrayList<>();
        List<Pair<String, String>> sectorList = new ArrayList<>();
        if (remote_db){
            siteList = db.getSites(null);
        } else {
            siteList.clear();
        }

        final DropdownListAdapter siteAdapter = new DropdownListAdapter(this);
        siteAdapter.addItems(siteList);
        siteSpinner.setAdapter(siteAdapter);

        if (remote_db){
            site = ((Pair<String, String>) siteAdapter.getItem(0)).first;
            sectorList = db.getSectors(site);
        } else {
            sectorList.clear();
        }

        final DropdownListAdapter sectorAdapter = new DropdownListAdapter(this);
        sectorAdapter.addItems(sectorList);
        sectorSpinner.setAdapter(sectorAdapter);

        siteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                site = ((Pair<String, String>) siteAdapter.getItem(position)).first;
                Log.d("siteSpinner: ", "Site selected is " + site);
                if (sampleService != null) {
                    int i, n;
                    sampleService.onSiteChanged(site);
                    sectorAdapter.clear();
                    sectorAdapter.addItems(db.getSectors(site));
                    n=sectorAdapter.getCount();
                    Log.d("siteSpinner", "Number of items in sectorAdapter: " + n);
                    for (i=0; i<n; i++)
                    {
                        Log.d("siteSpinner",  "sectorList ["+i+"] = "+ sectorAdapter.getItem(i));
                    }
                    if(sectorAdapter.getCount() > 0) {
                        sector = ((Pair<String, String>) sectorAdapter.getItem(0)).first;
                        Log.d("siteSpinner: ", "Default sector is the first sector: " + sector);
                        if (sampleService != null) {
                            sampleService.onSectorChanged(sector);
                        }
                    }
                    sectorAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sector = ((Pair<String, String>) sectorAdapter.getItem(position)).first;
                Log.d("sectorSpinner: ", "Position is : "+ position +" sector selected is " + sector);
                if (sampleService != null) {
                    sampleService.onSectorChanged(sector);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d("sectorSpinner: ", "Number of items in sectorAdapter: "+sectorAdapter.getCount());
                if(sectorAdapter.getCount() > 0) {
                    sector = ((Pair<String, String>) sectorAdapter.getItem(0)).first;
                    Log.d("sectorSpinner: ", "onNothingSelected. Position is :  0,  sector selected is " + sector);
                    if (sampleService != null) {
                        sampleService.onSectorChanged(sector);
                    }
                }
            }
        });

        spotET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                spot = s.toString();
                if (sampleService != null) {
                    sampleService.onSpotChanged(spot);
                }
            }
        });
    }

    public void takePicture(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(SensorSampleActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    1);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(SensorSampleActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(SensorSampleActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            return;
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        File tempFile = null;
        try {
            tempFile = createTempFile();
            tempFile.delete();
        } catch (Exception e){
            e.printStackTrace();
        }
        pictureUri = Uri.fromFile(tempFile);
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
        startActivityForResult(i, 0);
    }

    public void addNotes(View v) {
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.alert_sample_notes, null);
        final EditText notes = dialog.findViewById(R.id.sampleNotes);

        Dialog d = new AlertDialog.Builder(SensorSampleActivity.this)
                .setNegativeButton("Cancel", null)
                .setTitle("Add Sample Notes")
                .setView(dialog)
                .setPositiveButton("Save Notes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sampleService.setSampleNotes(notes.getText().toString());
                    }
                })
                .show();
    }

    // By default android pictures using the Media.IMAGE_CAPTURE are not full size when returning from
    // the activity. I crete a temporary directory to save the file in and retrieve it later.
    private File createTempFile() throws Exception {
        File temp = new File(baseDir, "tmp");
        if (!temp.exists()){
            temp.mkdir();
        }
        return File.createTempFile("picture", ".png", temp);
    }

    private class saveImage extends AsyncTask<Bitmap,Void,Boolean>{
        Bitmap b = null;

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(context);
            progress.setMessage("Please wait. Saving photo.");
            progress.setCancelable(false);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
        }

        @Override
        protected Boolean doInBackground(Bitmap... params){
            b = params[0];
            try {
                File sd = Environment.getExternalStorageDirectory();

                if (sd.canWrite()) {
                    File baseDir = new File(directory);
                    if (!baseDir.exists()){
                        baseDir.mkdir();
                    }
                    File picturesDir = new File(baseDir, "pictures");
                    if (!picturesDir.exists()) {
                        picturesDir.mkdir();
                    }
                    int count = 0;
                    for (File f : Objects.requireNonNull(picturesDir.listFiles())){
                        if (f.getName().startsWith(pictureName)){
                            count += 1;
                        }
                    }

                    File picture = new File(picturesDir, pictureName + ".png");
                    if (picture.exists()) {
                        if (count > 0) {
                            picture = new File(picturesDir, pictureName + "-" + count + ".png");
                        }
                    }

                    FileOutputStream fOut = new FileOutputStream(picture);
                    b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();

                    MediaStore.Images.Media.insertImage(context.getContentResolver(),
                            picture.getAbsolutePath(), picture.getName(), picture.getName());
                    sampleService.setPictureName(picture.getName());
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return null;
        }

        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast t = Toast.makeText(context, "Successfully saved Image", Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            } else {
                Log.e("SensorSampleActivity", "Error saving bitmap");
                Toast t = Toast.makeText(context, "Something went wrong, try again.", Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            }
            progress.dismiss();
        }
    }

    public void calibrateSensor(View v){
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.alert_calibration_value, null);
        final EditText value = dialog.findViewById(R.id.calibrationValue);
        final Spinner dropdown = dialog.findViewById(R.id.sensorSpinner);
        ArrayList<String> sensorCalib = new ArrayList<>();
        for (aSensor s : sensors){
            sensorCalib.add(s.getName());
        }
        sampleService.controlCalibrating(true);
        final ArrayAdapter<String> sensorAdapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_item, sensorCalib);
        sensorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(sensorAdapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tmp = sensorAdapter.getItem(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Dialog d = new androidx.appcompat.app.AlertDialog.Builder(SensorSampleActivity.this)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sampleService.controlCalibrating(false);
                    }
                })
                .setTitle("Insert value for calibration")
                .setView(dialog)
                .setPositiveButton("Calibrate", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sampleService.calibrateSensor(Float.valueOf(value.getText().toString()), tmp);
                        dialog.dismiss();
                    }
                })
                .show();
    }

}
