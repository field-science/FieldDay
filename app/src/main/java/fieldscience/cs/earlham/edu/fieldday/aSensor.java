package fieldscience.cs.earlham.edu.fieldday;

import android.hardware.Sensor;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import static java.lang.Math.round;

public class aSensor implements Parcelable {

    private float[] lastValues;
    private float quality;
    private Integer numberValues;
    private final String type;
    private String platform;
    private String name;
    private String id;
    private Sensor sensor;

    // Using Built In sensors
    public aSensor(Sensor s) {
        sensor = s;
        type = sensor.getStringType();
        name = type;
        platform = "Android";
        lastValues = null;
        numberValues = 0;
    }

    // Using bluetooth connected sensor
    public aSensor(String sensorID, String name) {
        id = sensorID;
        this.name = name;
        type = name;
        lastValues = null;
        numberValues = 1;
    }

    // Created from Parcel
    private aSensor(String sensorID, String name, float[] values, float quality, String platform, int numberValues){
        id = sensorID;
        this.name = name;
        type = name;
        lastValues = values;
        this.quality = quality;
        this.numberValues = numberValues;
        this.platform = platform;
    }

    @NonNull
    public String toString() {
        return sensor.toString();
    }

    public String getName() { return name; }

    public float[] getLastValues() { return lastValues; }

    public int getNumberValues() {return numberValues; }

    public String getPlatform() { return platform; }

    public String getID() { return id; }

    public String getStringType() { return type; }

    // To me this is a kludge of sorts but it appears to be the "accepted" way of rounding floats in Java to N
    // decimal places. If you want less or more change by a factor of 10.
    public String getLastValueString() {
        float displayWidth = 10000.0f;
        StringBuilder result = new StringBuilder(Float.toString(round(lastValues[0] * displayWidth) / displayWidth));

        for (int i=1; i<numberValues; i++) {
            result.append(",").append(round(lastValues[i] * displayWidth) / displayWidth);
        }

        return result.toString();
    }

    public void setLastValue(float[] val, int numArguments) {
        lastValues = val;
        numberValues = numArguments;
        //Log.d("setLastValue", "name = "+name + ", numberValues = " + numArguments + ", val = "+val);
    }

    public void setLastValueQuality(float quality){
        this.quality = quality;
    }

    public float getLastValueQuality(){
        return quality;
    }

    public void setID(String id){
        this.id = id;
    }

    public void setPlatform(String platformName){ platform = platformName; }

    public void setName(String sensorName){
        name = sensorName;
    }

    // Required by Android Parcelable class
    @Override
    public int describeContents() {
        return 0;
    }

    // Required by Android Parcelable class
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeFloatArray(lastValues);
        dest.writeFloat(quality);
        dest.writeString(platform);
        dest.writeInt(numberValues);
    }

    public static final Parcelable.Creator<aSensor> CREATOR = new Parcelable.Creator<aSensor>() {
        public aSensor createFromParcel(Parcel in) {
            String sensorID = in.readString();
            String sensorName = in.readString();
            float[] values = in.createFloatArray();
            float quality = in.readFloat();
            String platform = in.readString();
            int numberValues = in.readInt();
            return new aSensor(sensorID, sensorName, values, quality, platform, numberValues);
        }
        public aSensor[] newArray(int size) {
            return new aSensor[size];
        }
    };

}

