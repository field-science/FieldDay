package fieldscience.cs.earlham.edu.fieldday;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class ReadingsDatabase extends SQLiteOpenHelper {

    // Name of database
    private static final String DB_NAME = "readings.db";

    private static final String TAG = ReadingsDatabase.class.getSimpleName();

    // Name of tables
    private static final String T_TRIP = "fieldday_trip";
    private static final String T_SITE = "fieldday_site";
    private static final String T_SECTOR = "fieldday_sector";
    private static final String T_SPOT = "fieldday_spot";
    public static final String T_STREAM = "fieldday_streaming";
    public static final String T_READING = "fieldday_reading";
    private static final String T_HOST = "fieldday_host";
    private static final String T_PLATFORM = "fieldday_platform";
    private static final String T_SENSOR = "fieldday_sensor";
    private static final String T_UPLOAD = "fieldday_upload";

    // Name of columns
    private static final String C_TRIPID = "tripID";
    private static final String C_TRIPNAME = "tripName";
    private static final String C_SITEID = "siteID";
    private static final String C_SITENAME = "siteName";
    private static final String C_SITENOTES = "siteNotes";
    private static final String C_SECTORID = "sectorID";
    private static final String C_SECTORNAME = "sectorName";
    private static final String C_SECTORNOTES = "sectorNotes";
    private static final String C_SPOTID = "spotID";
    private static final String C_SPOTIMAGEFOLDER = "spotImageFolder";
    private static final String C_SPOTNOTES = "spotNotes";
    private static final String C_SENSORID = "sensorID";
    private static final String C_SENSORTYPE = "sensorType";
    private static final String C_SENSORUNITS = "sensorUnits";
    private static final String C_HOSTID = "hostID";
    private static final String C_PLATFORMID = "platformID";
    private static final String C_PLATFORMNAME = "platformName";
    private static final String C_PLATFORMTYPE = "platformType";
    private static final String C_RECORDTIME = "recordTime";
    private static final String C_VALUE = "value_";
    //public static final String C_VALUE_ONE = "value_1";
    //public static final String C_VALUE_TWO = "value_2";
    //public static final String C_VALUE_THREE = "value_3";
    private static final String C_QUALITY = "quality";
    private static final String C_READINGNOTES = "readingNotes";
    private static final String C_IMAGEFILENAME = "imageFilename";
    private static final String C_LATITUDE = "latitude";
    private static final String C_LONGITUDE = "longitude";
    private static final String C_ELEVATION = "elevation";
    private static final String C_ACCURACY = "accuracy";
    private static final String C_SATELLITES = "satellites";
    //upload table
    private static final String C_UPLOADID = "uploadid";
    private static final String C_READCOUNT = "reading_count";
    private static final String C_STREAMCOUNT = "streaming_count";

    public Context context;

    private static final String host = Build.SERIAL;
    private static final int DATABASE_VERSION = 1;
    private static File dbPath;
    public static final String[] tables = {T_HOST, T_TRIP, T_SITE, T_SECTOR, T_SPOT, T_PLATFORM, T_SENSOR, T_UPLOAD};
    public static final String FLOAT = "float";
    public static final String INT = "integer";
    public static final String TEXT = "text";
    public static final String VARCHAR = "varchar";
    public static final String TIMESTAMP = "timestamp";
    public static final String TIMESTAMPTZ = "timestamptz";


    // Array separating columns by type. This is used when updating from remote database;
    private static final String[] text_columns = {C_SITENOTES, C_SECTORNOTES, C_READINGNOTES,
                                            C_SPOTNOTES, C_IMAGEFILENAME};
    private static final String[] varchar_columns = {C_HOSTID, C_TRIPNAME, C_SITENAME, C_SECTORNAME, C_SPOTIMAGEFOLDER,
                                               C_PLATFORMID, C_PLATFORMNAME, C_PLATFORMTYPE, C_SENSORID,
                                                C_SENSORID, C_SENSORTYPE, C_SENSORUNITS};
    private static final String[] integer_columns = {C_TRIPID, C_SITEID, C_SECTORID, C_SPOTID, C_SATELLITES, C_UPLOADID, C_READCOUNT, C_STREAMCOUNT};
    private static final String[] float_columns = {C_QUALITY, C_LATITUDE, C_LONGITUDE, C_ELEVATION, C_ACCURACY};
    //C_VALUE_ONE, C_VALUE_TWO, C_VALUE_THREE,

    private static ReadingsDatabase sInstance = null;
    public static final String directory = Environment.getExternalStorageDirectory().toString() +
            File.separator + "FieldDay";
    public static synchronized ReadingsDatabase getInstance(Context context){
        if (sInstance == null){
            sInstance = new ReadingsDatabase(context.getApplicationContext());
        }
        return sInstance;
    }

    public ReadingsDatabase(Context c){
        super(c, DB_NAME, null, DATABASE_VERSION);
        dbPath = c.getDatabasePath(DB_NAME);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_host = "create table if not exists " + T_HOST + "(" +
                "hostID varchar(30) not null," +
                "primary key(hostID));";
        db.execSQL(create_host);

        String create_trip = "create table if not exists " + T_TRIP + "(" +
                "tripID int not null," +
                " tripName varchar(128)," +
                " primary key(tripID));";
        db.execSQL(create_trip);

        String create_site = "create table if not exists " + T_SITE + "(" +
                "tripID int not null," +
                " siteID int not null," +
                " siteName varchar(128)," +
                " siteNotes text," +
                " primary key(tripID, siteID)," +
                " foreign key(tripID) references fieldday_trip(tripID));";
        db.execSQL(create_site);

        String create_sector = "create table if not exists " + T_SECTOR + "(" +
                "tripID int not null," +
                " siteID int not null," +
                " sectorID int not null," +
                " sectorName varchar(128) not null," +
                " sectorNotes text," +
                " primary key(tripID, siteID, sectorID)," +
                " foreign key(tripID, siteID) references fieldday_site(tripID, siteID));";
        db.execSQL(create_sector);

        String create_spot = "create table if not exists " + T_SPOT + "(" +
                "tripID int not null," +
                " siteID int not null," +
                " sectorID int not null," +
                " spotID int not null," +
                " spotImageFolder varchar(128)," +
                " spotNotes text," +
                " primary key(tripID, siteID, sectorID, spotID)," +
                " foreign key(tripID, siteID, sectorID) references fieldday_sector(tripID, siteID, sectorID));";
        db.execSQL(create_spot);

        String create_platform = "create table if not exists " + T_PLATFORM + "(" +
                "platformID varchar(8) unique not null," +
                " platformType varchar(16) not null," +
                " platformName varchar(16) not null," +
                " primary key(platformID));";
        db.execSQL(create_platform);

        String create_sensor = "create table if not exists " + T_SENSOR + "(" +
                "platformID varchar(8) not null," +
                " sensorID varchar(8) unique not null," +
                " sensorType varchar(30) not null," +
                " sensorUnits varchar(8) not null, " +
                " primary key(sensorID), " +
                " foreign key(platformID) references fieldday_platform(platformID));";
        db.execSQL(create_sensor);

        String create_streaming = "create table if not exists " + T_STREAM + "(" +
                "tripID int not null," +
                " siteID int not null," +
                " sectorID int not null," +
                " platformID varchar(8) not null," +
                " sensorID varchar(8) not null," +
                " hostID varchar(30) not null," +
                " recordTime timestamptz not null," +
                " value_1 decimal not null," +
                " quality float not null," +
                " latitude float not null," +
                " longitude float not null," +
                " elevation float not null," +
                " accuracy float not null," +
                " satellites int not null," +
                " value_2 decimal," +
                " value_3 decimal," +
                " value_4 decimal," +
                " value_5 decimal," +
                " value_6 decimal," +
                //check if these keys are correct (e.g. recordtime sensible for a stream?)
                " primary key(tripID, siteID, sectorID, hostID, sensorID, recordTime)," +
                " foreign key(tripID, siteID, sectorID) references fieldday_sector(tripID, siteID, sectorID)," +
                " foreign key(hostID) references fieldday_host(hostID), " +
                " foreign key(platformID, sensorID) references fieldday_sensor(platformID, sensorID));";
        db.execSQL(create_streaming);

        String create_reading = "create table if not exists " + T_READING + "(" +
                "tripID int not null," +
                " siteID int not null," +
                " sectorID int not null," +
                " spotID int not null," +
                " hostID varchar(30) not null, " +
                " platformID varchar(8) not null," +
                " sensorID varchar(8) not null," +
                " recordTime timestamptz not null," +
                " value_1 decimal not null," +
                " quality float not null," +
                " latitude float not null," +
                " longitude float not null," +
                " elevation float not null," +
                " accuracy float not null," +
                " satellites int not null," +
                " readingNotes text," +
                " imageFilename text," +
                " value_2 decimal," +
                " value_3 decimal," +
                " value_4 decimal," +
                " value_5 decimal," +
                " value_6 decimal," +
                " primary key(tripID, siteID, sectorID, spotID, hostID, sensorID, recordTime)," +
                " foreign key(tripID, siteID, sectorID, spotID) references fieldday_spot(tripID, siteID, sectorID, spotID)," +
                " foreign key(hostID) references fieldday_host(hostID)," +
                " foreign key(platformID, sensorID) references fieldday_sensor(platformID, sensorID));";
        db.execSQL(create_reading);

        String create_upload = "create table if not exists " + T_UPLOAD + "(" +
                "uploadid int not null," +
                "hostid varchar(30)," +
                "starttime timestamptz," +
                "endtime timestamptz," +
                "reading_count int," +
                "streaming_count int," +
                "primary key(uploadid));";
        db.execSQL(create_upload);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        for (String s : tables) {
            db.execSQL("drop table if exists " + s + ";");
        }
        onCreate(db);
    }

    public boolean addReading(aSensor sensor, float[] sensorValues, float sensorQuality, String timestamp,
                           String site, String sector, String spot, double[] geoInfo, String trip,
                           float accuracy, int satellites, String readingNotes, String imageFilename,
                           String table) {
        DecimalFormat d = new DecimalFormat("#.####");

        boolean success;
        SQLiteDatabase dbRead = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        Cursor c;

        int i;
        int numberValues;
        String c_value_i;

        c = dbRead.query(T_TRIP, new String[]{C_TRIPID}, "tripName = ?",
                new String[]{trip}, null, null, null);
        c.moveToFirst();
        cv.put(C_TRIPID, c.getInt(c.getColumnIndex("tripID")));
        c.close();
        c = dbRead.query(T_SECTOR, new String[]{C_SECTORID}, "sectorName = ?",
                new String[]{sector}, null, null, null);
        c.moveToFirst();
        cv.put(C_SECTORID, c.getInt(c.getColumnIndex("sectorID")));
        c.close();

        c = dbRead.query(T_SITE, new String[]{C_SITEID}, "siteName = ?",
                new String[]{site}, null, null, null);
        c.moveToFirst();
        cv.put(C_SITEID, c.getInt(c.getColumnIndex("siteID")));
        c.close();

        // only the readings table cares about spots
        if (table.equals("fieldday_reading")) {
            cv.put(C_SPOTID, Integer.parseInt(spot));
            cv.put(C_READINGNOTES, readingNotes);
            cv.put(C_IMAGEFILENAME, imageFilename);
        }

        c = dbRead.query(T_PLATFORM, new String[]{C_PLATFORMID}, "platformName = ?",
                new String[]{sensor.getPlatform()}, null, null, null);
        c.moveToFirst();
        String platformID = c.getString(c.getColumnIndexOrThrow("platformID"));
        cv.put(C_PLATFORMID, platformID);
        c.close();

        dbRead.close();

        cv.put(C_SENSORID, sensor.getID());
        cv.put(C_RECORDTIME, timestamp);

        numberValues = sensor.getNumberValues();
        Log.d("ReadingDatabase", "numberValues = " + numberValues);
        for(i=0; i<numberValues; i++)
        {
            Log.d("ReadingsDatabase", " sensorValues ["+ i+ "] = " + sensorValues[i]);
            if(sensorValues[i] != 0.0f){
                c_value_i = C_VALUE + (i + 1);
                Log.d("ReadingsDatabase", "c_value_i = " + c_value_i);
                cv.put(c_value_i, Double.valueOf(d.format(sensorValues[i])));
            }
        }
        /*
        cv.put(C_VALUE_ONE, Double.valueOf(d.format(sensorValues[0])));
        //pressure sensor has only one value, the following two if statements are running only when the length is greater than 2
        //check the number of values for each sensor, and then determine if you need to access the 2nd and 3rd value
        if(numberValues > 1) {
            if (sensorValues[1] != 0.0f) {
                cv.put(C_VALUE_TWO, Double.valueOf(d.format(sensorValues[1])));
            }
            if(numberValues > 2){
                if (sensorValues[2] != 0.0f) {
                    cv.put(C_VALUE_THREE, Double.valueOf(d.format(sensorValues[2])));
                }
            }
        }
        */
        cv.put(C_QUALITY, sensorQuality);
        cv.put(C_LATITUDE, geoInfo[0]);
        cv.put(C_LONGITUDE, geoInfo[1]);
        cv.put(C_ELEVATION, geoInfo[2]);
        cv.put(C_ACCURACY, accuracy);
        cv.put(C_SATELLITES, satellites);
        cv.put(C_HOSTID, host);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.insert(table, null, cv);
            success = true;
        } catch (SQLiteException e){
            e.printStackTrace();
            success = false;
        }
        db.close();
        return success;
    }

    public boolean addSpot(String trip, String site, String sector, String spot){
        SQLiteDatabase dbRead = this.getReadableDatabase();
        ContentValues cv = new ContentValues();
        boolean success = false;
        cv.put(C_SPOTIMAGEFOLDER, "");
        cv.put(C_SPOTNOTES, "");
        cv.put(C_SPOTID, Integer.parseInt(spot));
        Cursor c = dbRead.query(T_SECTOR, new String[]{C_SECTORID}, "sectorName = ?",
                new String[]{sector}, null, null, null);
        c.moveToFirst();
        int sectorID = c.getInt(c.getColumnIndex("sectorID"));
        cv.put(C_SECTORID, sectorID);
        c.close();

        c = dbRead.query(T_SITE, new String[]{C_SITEID}, "siteName = ?",
                new String[]{site}, null, null, null);
        c.moveToFirst();
        int siteID = c.getInt(c.getColumnIndex("siteID"));
        cv.put(C_SITEID, siteID);
        c.close();

        c = dbRead.query(T_TRIP, new String[]{C_TRIPID}, "tripName = ?",
                new String[]{trip}, null, null, null);
        c.moveToFirst();
        int tripID = c.getInt(c.getColumnIndex("tripID"));
        cv.put(C_TRIPID, tripID);
        c.close();

        c = dbRead.query(T_SPOT, new String[]{C_SPOTID},
                "spotID = ? and sectorID = ? and siteID = ? and tripID = ?",
                new String[]{spot, Integer.toString(sectorID), Integer.toString(siteID), Integer.toString(tripID)},
                null, null, null);
        if(c.getCount() == 0){
            try {
                c.close();
                dbRead.close();
                SQLiteDatabase db = this.getWritableDatabase();
                db.insert(T_SPOT, null, cv);
                success = true;
                db.close();
            } catch (SQLiteConstraintException e) {
                e.printStackTrace();
            }
        }
        return(success);
    }

    public void cleanDatabase(String tableToClean){
        Log.d("TABLECLEAN","table to clean: "+tableToClean);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("select endtime from fieldday_upload order by endtime desc",null);
        if(c==null || c.getCount()==0){
            Log.d("TABLECLEAN","Nothing to clean here");
            return;
        }
        String mostRecentUploadTS;
        c.moveToFirst();
        mostRecentUploadTS = "'" + c.getString(c.getColumnIndex("endtime")) + "'";
        Log.d("TABLECLEAN","last upload: " + mostRecentUploadTS);
        String laterThanCondition = "recordtime > ?";
        String[] mostRecentUploadArray = {mostRecentUploadTS};
        //onCreate(db);

        switch (tableToClean){
            case "streaming":
                Log.d("TABLECLEAN", "You want to clean in-field streaming");
                db.delete(T_STREAM,laterThanCondition, mostRecentUploadArray);
                break;
            case "reading":
                Log.d("TABLECLEAN","You want to clean in-field readings");
                db.delete(T_READING,laterThanCondition, mostRecentUploadArray);
                //db.execSQL("delete from " + T_SPOT + ";");
                break;
            case "field":
                Log.d("TABLECLEAN","You want to clean all records collected in the field");
                db.delete(T_STREAM,laterThanCondition, mostRecentUploadArray);
                db.delete(T_READING,laterThanCondition, mostRecentUploadArray);
                break;
            case "all":
                Log.d("TABLECLEAN", "You want to clean all the tables");
                for (String table : tables){
                    db.execSQL("delete from " + table + ";");
                }
                db.execSQL("delete from " + T_READING + ";");
                db.execSQL("delete from " + T_STREAM + ";");
                break;
        }
        onCreate(db);
        c.close();
    }

    public boolean copyDatabase(String directoryPath){
        boolean success = false;
        String path;
        try {
            File sd = Environment.getExternalStorageDirectory();
            if (sd.canWrite()) {
                File baseDir = new File(directory);
                if (!baseDir.exists()){
                    baseDir.mkdir();
                }

                if (!directoryPath.equals("")) {
                    path = directory + '/' + directoryPath;
                } else {
                    path = directory;
                }


                File backupDB = new File(path);
                if (!backupDB.exists()) {
                    backupDB.mkdir();
                }

                SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd-HH:mm:ss", Locale.ENGLISH);
                Calendar c = Calendar.getInstance();
                String timestamp = date.format(c.getTime());

                File externalDBfile = new File(backupDB, timestamp+"-"+DB_NAME);
                if (dbPath.exists()) {
                    FileChannel src = new FileInputStream(dbPath).getChannel();
                    FileChannel dst = new FileOutputStream(externalDBfile).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    success = true;
                }
            }
        } catch (Exception e) {
            success = false;
        }
        return(success);
    }

    public String getColumnType(String column){
        if (column.contains(C_VALUE))//check if it is sensor value
        {
            return (FLOAT);
        }else if (Arrays.asList(ReadingsDatabase.float_columns).contains(column)) {
            return (FLOAT);
        } else if (Arrays.asList(ReadingsDatabase.text_columns).contains(column)) {
            return (TEXT);
        } else if (Arrays.asList(ReadingsDatabase.integer_columns).contains(column)) {
            return(INT);
        } else if (Arrays.asList(ReadingsDatabase.varchar_columns).contains(column)) {
            return(VARCHAR);
        } else {
            return(TIMESTAMP);
        }
    }

    public Cursor query(String tableName, String whereArgs, String[] whereParams){
        SQLiteDatabase db = this.getReadableDatabase();
        return (db.query(tableName, getColumns(tableName), whereArgs, whereParams,
                null, null, null));
    }

    public String[] getColumns(String tableName){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(tableName, null, null, null, null, null, null);
        String[] columns = c.getColumnNames();
        c.close();
        return(columns);
    }

    public void updateFromRemoteDB(ContentValues cv, String table){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(table, null, cv);
        db.close();
    }

    public List<String> getSpots(String site, String sector){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = null;
        List<String> values = new ArrayList<>();
        if ((site != null) && (sector != null)){
            c = db.query(T_SECTOR, new String[]{C_SECTORID}, "sectorName = ?", new String[]{sector},
                    null, null, null);
            c.moveToFirst();
            int sectorid = c.getInt(c.getColumnIndex(C_SECTORID));
            c.close();
            c = db.query(T_SITE, new String[]{C_SITEID}, "siteName = ?", new String[]{site},
                    null, null, null);
            c.moveToFirst();
            int siteid = c.getInt(c.getColumnIndex(C_SITEID));
            c.close();
            c = db.query(T_SPOT, new String[] {C_SPOTID}, "siteID = ? and sectorID = ?",
                    new String[]{Integer.toString(siteid), Integer.toString(sectorid)},
                    null, null, null);
        }
        while(Objects.requireNonNull(c).moveToNext()){
            values.add(Integer.toString(c.getInt(c.getColumnIndex(C_SPOTID))));
        }
        c.close();
        return values;
    }

    public List<Pair<String, String>> getSectors(String site){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c;
        List<Pair<String, String>> values = new ArrayList<>();
        if (site != null){
            c = db.query(T_SITE, new String[]{C_SITEID}, "siteName = ?", new String[]{site},
                    null, null, null);
            c.moveToFirst();
            int id = c.getInt(c.getColumnIndex(C_SITEID));
            c.close();
            c = db.query(T_SECTOR, new String[]{C_SECTORNAME, C_SECTORID}, "siteID = ?", new String[]{Integer.toString(id)},
                    null, null, null);
        } else {
            c = db.query(T_SECTOR, new String[]{C_SECTORNAME, C_SECTORID}, null, null, null, null, null);
        }
        while (c.moveToNext()) {
            values.add(Pair.create(
                    c.getString(c.getColumnIndex(C_SECTORNAME)), Integer.toString(c.getInt(c.getColumnIndex(C_SECTORID)))));
        }
        c.close();
        return values;
    }

    public List<Pair<String, String>> getSites(String trip){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(T_SITE, new String[]{C_SITENAME, C_SITEID}, null, null, null, null, null);
        List<Pair<String, String>> values = new ArrayList<>();
        while (c.moveToNext()) {
            values.add(Pair.create(
                    c.getString(c.getColumnIndex(C_SITENAME)), Integer.toString(c.getInt(c.getColumnIndex(C_SITEID)))));
        }
        c.close();
        return values;
    }

    public List<String> getPlatforms(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(T_PLATFORM, new String[]{C_PLATFORMNAME}, null, null, null, null, null);
        List<String> values = new ArrayList<>();
        while (c.moveToNext()){
            values.add(c.getString(c.getColumnIndex("platformName")));
        }
        c.close();
        return values;
    }

    public List<Pair<String,String>> getSensors(String platform){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Pair<String,String>> values = new ArrayList<>();
        Cursor c;
        if (platform != null){
            c = db.query(T_PLATFORM, new String[]{C_PLATFORMID}, "platformName = ?", new String[]{platform},
                    null, null, null);
            c.moveToFirst();
            String platformId = c.getString(c.getColumnIndex(C_PLATFORMID));
            Log.d(TAG, platformId);
            c.close();
            c = db.query(T_SENSOR, new String[]{C_SENSORTYPE, C_SENSORID}, "platformID = ?", new String[]{platformId},
                    null, null, null);
        } else {
            c = db.query(T_SENSOR, new String[]{C_SENSORTYPE, C_SENSORID}, null, null, null, null, null);
        }
        while (c.moveToNext()){
            Log.d(TAG, Integer.toString(c.getCount()));
            values.add(Pair.create(
                    c.getString(c.getColumnIndex("sensorType")), c.getString(c.getColumnIndex("sensorID"))));
        }
        c.close();
        return values;
    }

    public long getSize() {
        return dbPath.length();
    }

    public int getStatus() {
        int readings = query(T_READING, null, null).getCount();
        Log.d("ReadingsDatabase", "Readings Counts:" + readings);
        int streaming = query(T_STREAM, null, null).getCount();
        Log.d("ReadingsDatabase", "Streamings Counts:" + streaming);

        // Empty database
        if (getSize() == 0){
           return 0;
        } else if (getSites(null).size() != 0) {
            if (readings != 0) {
                if (streaming == 0) {
                    // Just readings populated
                    return 1;
                } else {
                    return 2;
                }
            } else {
                if (streaming != 0) {
                    return 4;
                } else {
                    return 3;
                }
                // Initialized but no readings
            }
        }
        return 0;
    }
}
