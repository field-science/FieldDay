package fieldscience.cs.earlham.edu.fieldday;

import java.util.HashMap;

class GattAttributes {
    private static final HashMap<String, String> attributes = new HashMap();
    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    public static final String BLE_SENSOR_SERVICE = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
    public static final String BLE_SENSOR_VALUE_RX = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";
    public static final String BLE_SENSOR_REQUEST_TX = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
    private static final String BLE_DEVICE_INFO = "0000180a-0000-1000-8000-00805f9b34fb";

    static {
        attributes.put(BLE_DEVICE_INFO, "Device Information Service");
        attributes.put(BLE_SENSOR_REQUEST_TX, "Request Sensor Value");
        attributes.put(BLE_SENSOR_SERVICE, "Sensor Service");
        attributes.put(BLE_SENSOR_VALUE_RX, "Sensor Value");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
