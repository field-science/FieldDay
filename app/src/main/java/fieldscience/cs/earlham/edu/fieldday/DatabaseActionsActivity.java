package fieldscience.cs.earlham.edu.fieldday;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class DatabaseActionsActivity extends AppCompatActivity {

    private static final String DB_DRIVER = "org.postgresql.Driver";
    private static final String[] uploadTables = {"fieldday_spot", "fieldday_reading", "fieldday_streaming"};

    private String uname, database, pass, host, pNum;
    private static final String hostid = Build.SERIAL;

    private static String trip;
    private static int tripID;
    private static int downloadColumnCount = 0;
    private static int uploadColumnCount = 0;
    private TextView cleanTV, uploadTV, downloadTV, dbSizeTV, dbStatusTV;
    private String[] tripArray;
    private String[] tripIds;
    private Resources res;
    private View button;
    private Connection connection;
    private SharedPreferences.Editor editor;
    private ReadingsDatabase db;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_actions);
        res = getResources();
        connection = null;
        pNum = "5432";

        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        db = new ReadingsDatabase(getApplicationContext());
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = settings.edit();

        for (String table : ReadingsDatabase.tables) {
            downloadColumnCount += db.getColumns(table).length;
        }

        for (String table : uploadTables) {
            uploadColumnCount += db.getColumns(table).length;
        }

        dbSizeTV = findViewById(R.id.databaseSize);
        dbSizeTV.setText(String.format(res.getString(R.string.databaseSize), Formatter.formatFileSize(this, db.getSize())));
        dbStatusTV = findViewById(R.id.databaseStatus);
        dbStatusTV.setText(String.format(res.getString(R.string.databaseStatus), ""));
        displayDatabaseStatus(db.getStatus());
        cleanTV = findViewById(R.id.lastClean);
        cleanTV.setText(String.format(res.getString(R.string.lastClean), settings.getString("lastClean", "Never")));
        uploadTV = findViewById(R.id.lastUpload);
        uploadTV.setText(String.format(res.getString(R.string.lastUpload), settings.getString("lastUpload", "Never")));
        downloadTV = findViewById(R.id.lastDownload);
        downloadTV.setText(String.format(res.getString(R.string.lastDownload), settings.getString("lastDownload", "Never")));


        EditText server = findViewById(R.id.serverIp);
        host = settings.getString("host", "cluster.earlham.edu");
        server.setText(host);
        editor.putString("host", host);
        editor.apply();
        server.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                host = s.toString();
                editor.putString("host", host);
                editor.apply();
            }
        });

        EditText dbName = findViewById(R.id.database);
        database = settings.getString("database_name", "field_science");
        dbName.setText(database);
        editor.putString("database_name", database);
        editor.apply();
        dbName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                database = s.toString();
                editor.putString("database_name", database);
                editor.apply();
            }
        });

        EditText username = findViewById(R.id.username);
        uname = settings.getString("username", "fieldsci");
        username.setText(uname);
        editor.putString("username", uname);
        editor.apply();
        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                uname = s.toString();
                editor.putString("username", uname);
                editor.apply();
            }
        });

        EditText password = findViewById(R.id.password);
        pass = settings.getString("password", "skalanes");
        password.setText(pass);
        editor.putString("password", pass);
        editor.apply();
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pass = s.toString();
                editor.putString("password", pass);
                editor.apply();
            }
        });

        EditText port = findViewById(R.id.port);
        pNum = settings.getString("port", "5432");
        port.setText(pNum);
        editor.putString("port", pNum);
        editor.apply();
        port.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                pNum = s.toString();
                editor.putString("port", pNum);
                editor.apply();
            }
        });

        Button setup = findViewById(R.id.setupDbButton);
        setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verify(v);
            }
        });

/*        Button clean = findViewById(R.id.wipeDatabase);
        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                verify(v);
            }
        });

        Button cleanReadings = findViewById(R.id.wipeReadingsTable);
        cleanReadings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                verify(v);
            }
        });

        Button cleanStreaming = findViewById(R.id.wipeStreamingTable);
        cleanStreaming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                verify(v);
            }
        });

        Button cleanField = findViewById(R.id.wipeFieldData);
        cleanField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                verify(v);
            }
        });*/

        Button upload = findViewById(R.id.uploadButton);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                verify(v);
            }
        });

        final Button cleanOptions = findViewById(R.id.cleanOptions);
        cleanOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

//                verify(v);
                PopupMenu popup = new PopupMenu(DatabaseActionsActivity.this, cleanOptions);
                popup.getMenuInflater()
                        .inflate(R.menu.cleaning_options, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DatabaseActionsActivity.this);
                        builder.setNegativeButton("Cancel", null);
                        builder.setTitle("Please confirm");
                        switch (item.getItemId()){
                            case R.id.cleanReadingTable:
                                if(db.copyDatabase("archive")){
                                    builder.setPositiveButton("Yes, I am sure.", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            db.cleanDatabase("reading");
                                            makeToast("The readings table is now clean");
                                            editor.putString("lastClean", getTimestamp());
                                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                                            displayDatabaseStatus(db.getStatus());
                                            editor.apply();
                                        }
                                    });
                                    builder.setMessage("You are about to clean recent data from reading. This will store an archived copy. Are you sure?");
                                    builder.create();
                                    builder.show();
                                }
                                break;
                            case R.id.cleanStreamingTable:
                                if(db.copyDatabase("archive")){
                                    builder.setPositiveButton("Yes, I am sure.", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            db.cleanDatabase("streaming");
                                            makeToast("The streaming table is now clean");
                                            editor.putString("lastClean", getTimestamp());
                                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                                            displayDatabaseStatus(db.getStatus());
                                            editor.apply();
                                        }
                                    });
                                    builder.setMessage("You are about to clean recent data from streaming. This will store an archived copy. Are you sure?");
                                    builder.create();
                                    builder.show();
                                }
                                break;
                            case R.id.cleanReadAndStreamTables:
                                if(db.copyDatabase("archive")){
                                    builder.setPositiveButton("Yes, I am sure.", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            db.cleanDatabase("field");
                                            makeToast("Both reading and streaming tables are now clean");
                                            editor.putString("lastClean", getTimestamp());
                                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                                            displayDatabaseStatus(db.getStatus());
                                            editor.apply();
                                        }
                                    });
                                    builder.setMessage("You are about to clear all recent data. Erasing the database will store an archived copy. Are you sure?");
                                    builder.create();
                                    builder.show();
                                }
                                break;
                            case R.id.eraseDatabase:
                                if(db.copyDatabase("archive")){
                                    builder.setPositiveButton("Yes, I am sure.", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    db.cleanDatabase("all");
                                                    makeToast("The database has been erased, set up a new one immediately");
                                                    editor.putString("lastClean", getTimestamp());
                                                    cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                                                    displayDatabaseStatus(db.getStatus());
                                                    //editor.apply();
                                                    editor.putString("trip","");
                                                    editor.apply();
                                                }
                                            });
                                    builder.setTitle("WARNING: You are about to erase the entire database");
                                    builder.setMessage("Erasing the database will store an archived copy and will require you to set up the database again. Are you sure?");
                                    builder.create();
                                    builder.show();
                                }
                                break;
                        }
                        editor.putString("lastClean", getTimestamp());
                        cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                        displayDatabaseStatus(db.getStatus());
                        editor.apply();
                        return true;
                    }
                });

                popup.show();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(0);
    }

    private void verify(final View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(DatabaseActionsActivity.this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(DatabaseActionsActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(DatabaseActionsActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            return;
        }
        switch(view.getId()){
            case R.id.setupDbButton:
                builder.setMessage("Are you sure that you want to setup your local database?");
                break;
            case R.id.uploadButton:
                builder.setMessage("Are you sure that you want to upload your tables? Doing this will move all of the " +
                        "data in the current local database to an archive folder.");
                break;
            case R.id.cleanOptions:
                builder.setMessage("Wiping the database will store an archived copy. Are you sure?");
                break;
        }
        builder.setTitle("Please verify");
        builder.setPositiveButton("Yes, I am sure.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (host == null || uname == null || pass == null || database == null) {
                    makeToast(res.getString(R.string.cannot_connect));
                } else {
                    Log.d("TABLECLEAN",Integer.toString(view.getId()));
                    button = findViewById(view.getId());
                    if (button == findViewById(R.id.eraseDatabase)) {
                        if(db.copyDatabase("archive")){
                            db.cleanDatabase("all");
                            makeToast("Local database content is now erased");
                            editor.putString("lastClean", getTimestamp());
                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                            displayDatabaseStatus(db.getStatus());
                            editor.apply();
                        }
                    }
                    else if (button == findViewById(R.id.cleanReadingTable)) {
                        if(db.copyDatabase("archive")){
                            db.cleanDatabase("reading");
                            makeToast("Readings table is now clean");
                            editor.putString("lastClean", getTimestamp());
                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                            displayDatabaseStatus(db.getStatus());
                            editor.apply();
                        }
                    }
                    else if (button == findViewById(R.id.cleanStreamingTable)) {
                        if(db.copyDatabase("archive")){
                            db.cleanDatabase("streaming");
                            makeToast("Streaming table is now clean");
                            editor.putString("lastClean", getTimestamp());
                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                            displayDatabaseStatus(db.getStatus());
                            editor.apply();
                        }
                    }
                    else if (button == findViewById(R.id.cleanReadAndStreamTables)) {
                        if(db.copyDatabase("archive")){
                            db.cleanDatabase("field");
                            makeToast("Both streaming and reading tables are now clean");
                            editor.putString("lastClean", getTimestamp());
                            cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                            displayDatabaseStatus(db.getStatus());
                            editor.apply();
                        }
                    }
                    else { new connectToDB().execute(); }
                    Log.d("TABLECLEAN","end of loop");
                }
            }
        });
        builder.setNegativeButton("Cancel.", null);
        builder.create();
        builder.show();
    }

    private class connectToDB extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String url = "jdbc:postgresql://" + host + ":" + pNum + "/" + database;
                connection = DriverManager.getConnection(url, uname, pass);
                if (connection != null) {
                    return true;
                }
            } catch (final SQLException e) {
                DatabaseActionsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        makeToast(e.toString());
                    }
                });
                e.printStackTrace();
                return false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result){
            if (result){
                switch(button.getId()){
                    case R.id.uploadButton:
                        progressDialog.setMessage("Upload local tables");
                        new uploadTables().execute();
                        progressDialog.show();
                        break;
                    case R.id.setupDbButton:
                        progressDialog.setMessage("Set up local database.");
                        new getTrips().execute();
                        break;
                }
            }
        }
    }

    private class getTrips extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            if (connection != null) {
                try {
                    Statement st = connection.createStatement();
                    ResultSet rs = st.executeQuery("select tripName, tripID from fieldday_trip");
                    List<String> trips = new ArrayList<>();
                    List<String> ids = new ArrayList<>();
                    while (rs.next()) {
                        trips.add(rs.getString("tripName"));
                        ids.add(Integer.toString(rs.getInt("tripID")));
                    }
                    tripArray = trips.toArray(new String[0]);
                    tripIds = ids.toArray(new String[0]);
                    DatabaseActionsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Dialog d = new androidx.appcompat.app.AlertDialog.Builder(DatabaseActionsActivity.this)
                                    .setTitle("Here are the trips I've discovered. Select your trip.")
                                    .setNegativeButton("No trip!", null)
                                    .setItems(tripArray, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            trip = tripArray[which];
                                            tripID = Integer.parseInt(tripIds[which]);
                                            editor.putString("trip", trip);
                                            editor.apply();
                                            new setupDB().execute();
                                        }
                                    })
                                    .create();
                            d.show();
                        }
                    });
                    rs.close();
                    st.close();
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return null;
        }

        protected void onPostExecute(Boolean result) {
        }
    }

    private class setupDB extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected void onPreExecute(){
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (connection != null) {
                try {
                    Statement st = connection.createStatement();
                    int total = 0;
                    for (String table : ReadingsDatabase.tables) {
                        Log.d("TABLECLEAN",table);
                        int count = 0;
                        String[] columns = db.getColumns(table);
                        String query;
                        if (Arrays.asList(columns).contains("tripID")) {
                            query = "select * from " + table + " where tripID = " + tripID + ";";
                        } else {
                            query = "select * from " + table + ";";
                        }
                        ResultSet rs = st.executeQuery(query);
                        ContentValues cv = new ContentValues();
                        int index = Arrays.asList(ReadingsDatabase.tables).indexOf(table);
                        while(rs.next()) {
                            count += 1;
                            for (String column : columns){
                                publishProgress(index, count);
                                switch(db.getColumnType(column)){
                                    case ReadingsDatabase.FLOAT:
                                        cv.put(column, rs.getFloat(column));
                                        break;
                                    case ReadingsDatabase.VARCHAR:
                                    case ReadingsDatabase.TEXT:
                                    case ReadingsDatabase.TIMESTAMP:
                                    case ReadingsDatabase.TIMESTAMPTZ:
                                        cv.put(column, rs.getString(column));
                                        break;
                                    case ReadingsDatabase.INT:
                                        cv.put(column, rs.getInt(column));
                                        break;
                                    default:
                                        break;
                                }
                            }
                            db.updateFromRemoteDB(cv, table);
                        }
                        rs.close();
                    }
                    st.close();
                    DatabaseActionsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            makeToast("Successfully copied the tables for trip: " + trip);
                        }
                    });
                    return true;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress){
            progressDialog.setMessage("Downloading table: " + ReadingsDatabase.tables[progress[0]]);
            double p = (progress[1]/ (double) downloadColumnCount) * 100.00;

            progressDialog.setProgress((int) p);
        }

        protected void onPostExecute(Boolean result) {
            if (result) {
                editor.putString("lastDownload", getTimestamp());
                downloadTV.setText(String.format(res.getString(R.string.lastDownload), getTimestamp()));
                displayDatabaseStatus(3);
                dbSizeTV.setText(String.format(res.getString(R.string.databaseSize), Formatter.formatFileSize(getApplicationContext(), db.getSize())));
                editor.apply();
            }
            progressDialog.dismiss();
        }
    }

    private class uploadTables extends AsyncTask<Void, Integer, Boolean> {
        final String[] tables = {"fieldday_spot", "fieldday_reading", "fieldday_streaming"};

        @Override
        protected Boolean doInBackground(Void... params) {
            if (connection != null) {
                try {
                    // AutoCommit set to false, so we can rollback if anything goes wrong
                    String startTS = getTimestamp();
                    connection.setAutoCommit(false);
                    Statement st = connection.createStatement();
                    int totalCount = 0;
                    int readingCount = 0, streamingCount = 0;
                    String uploadRecordQuery = "BEGIN WORK; LOCK TABLE fieldday_upload IN EXCLUSIVE MODE;";
                    st.executeUpdate(uploadRecordQuery);

                    // Get row count for total row count to be uploaded. This is used for setting the
                    // progress update
                    for (String table : tables){
                        int tableRows = db.query(table, null, null).getCount();
                        totalCount += tableRows;
                        if (table.equals("fieldday_reading")){readingCount = tableRows; }
                        else if (table.equals("fieldday_streaming")){ streamingCount = tableRows; }
                    }
                    Log.d("Total row count", Integer.toString(totalCount));
                    Log.d("Reading row count", Integer.toString(readingCount));
                    Log.d("Streaming row count", Integer.toString(streamingCount));

                    int count = 0;

                    for (String table : tables) {
                        String[] columns = db.getColumns(table);
                        // Query local sqlite table, all columns, all rows
                        Cursor c = db.query(table, null, null);
                        int index = Arrays.asList(tables).indexOf(table);
                        Log.d("Table index:", Integer.toString(index));
                        Log.d("Table:", table);
                        StringBuilder queryBuilder = new StringBuilder();

                        queryBuilder.append("insert into ").append(table).append(" (");
                        for (int i=0; i<columns.length; i++) {
                            String piece = c.getColumnName(i);
                            queryBuilder.append(piece).append(",");
                        }
                        queryBuilder.setLength(queryBuilder.length() - 1); //strip trailing comma
                        queryBuilder.append(") values ");
                        // fieldday_spot table has special characteristics so we handle that separately
                        if (table.equals("fieldday_spot")) {
                            while (c.moveToNext()) {
                                count += 1;
                                Log.d("Progress (row count):", Integer.toString(count));
                                publishProgress(index, (int) ((((double) count)/((double) totalCount))*100.0));
                                String q = String.format(res.getString(
                                        R.string.spotQuery),
                                        Integer.toString(c.getInt(c.getColumnIndex("tripID"))),
                                        Integer.toString(c.getInt(c.getColumnIndex("siteID"))),
                                        Integer.toString(c.getInt(c.getColumnIndex("sectorID"))),
                                        Integer.toString(c.getInt(c.getColumnIndex("spotID")))
                                );

                                // Make sure that there isn't a row with the trip, site, sector and spot
                                // as this row in the remote db. Only try to insert if count comes back
                                // as 0.
                                ResultSet rs = st.executeQuery(q);
                                // By default, the cursor is positioned on a row. This moves it to the
                                // next, which in this case is the first row.
                                rs.next();
                                // Make sure we aren't running into a primary key constraint.
                                if (rs.getInt("rowcount") == 0) {
                                    rs.close();
                                    for (int i=0; i<columns.length; i++) {
                                        String piece = getColumn(c, columns[i]);
                                        // I don't want to add an empty string, because that will
                                        // add extra commas that SQL doesn't like.
                                        if (piece.length() > 0){
                                            if (i == 0){
                                                queryBuilder.append("(").append(piece);
                                            } else if (i < columns.length-1) {
                                                queryBuilder.append(",").append(piece);
                                            } else {
                                                queryBuilder.append(",").append(piece).append(")");
                                            }
                                        }
                                    }
                                    queryBuilder.append(",");
                                }

                            }
                        } else {
                            // While there are rows in the table.
                            while (c.moveToNext()) {
                                count += 1;
                                Log.d("Progress (row count):", Integer.toString(count));
                                float countFraction = ((float) count / (float) totalCount);
                                publishProgress(index, (int) (countFraction) * 100);
                                for (int i = 0; i < columns.length; i++) {
                                    String piece = getColumn(c, columns[i]);
                                    if (piece.length() > 0) {
                                        // This series of if-else builds up the set of values to add to the db
                                        if (i == 0) {
                                            queryBuilder.append("(").append(piece);
                                        } else if (i < columns.length - 1) {
                                            queryBuilder.append(",").append(piece);
                                        } else {
                                            queryBuilder.append(",").append(piece).append(")");
                                        }
                                    }
                                }
                                queryBuilder.append(",");

                            }
                        }

                        queryBuilder.setLength(queryBuilder.length() - 1); //strip trailing comma
                        queryBuilder.append(";");
                        String query = queryBuilder.toString();
                        if (!(query.endsWith("values;"))) {
                            Log.d("QUERYINFO", query);
                            st.executeUpdate(query);
                            connection.commit();
                            Log.d("QUERYINFO","query done");
                        }

                    }
                    String endTS = getTimestamp();
                    uploadRecordQuery = "INSERT INTO fieldday_upload VALUES ("
                            + "default,"
                            + "'" + hostid  + "'" + ","
                            + "'" + startTS + "'" + ","
                            + "'" + endTS + "'" + ","
                            + readingCount + ","
                            + streamingCount
                            + "); ";
                    Log.d("UPLOADREC Full query", uploadRecordQuery);
                    st.executeUpdate(uploadRecordQuery);
                    connection.commit();
                    uploadRecordQuery = "COMMIT WORK";
                    st.executeUpdate(uploadRecordQuery);
                    connection.commit();
                    st.close();
                    return true;
                } catch (SQLException e) {
                    // Something went wrong trying to upload the files, so we try to rollback the
                    // transaction.
                    try {
                        connection.rollback();
                    } catch (SQLException ex){
                        Log.e("Rollback", Objects.requireNonNull(ex.getMessage()));
                    }
                    e.printStackTrace();
                    return false;
                }
            }
            return null;
        }

        String getColumn(Cursor c, String column) {
            switch (db.getColumnType(column)) {
                case ReadingsDatabase.FLOAT:
                    return(Float.toString(c.getFloat(c.getColumnIndex(column))));
                case ReadingsDatabase.VARCHAR:
                case ReadingsDatabase.TEXT:
                    return("'" + c.getString(c.getColumnIndex(column)) + "'");
                case ReadingsDatabase.INT:
                    return(Integer.toString(c.getInt(c.getColumnIndex(column))));
                case ReadingsDatabase.TIMESTAMP:
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Locale.ENGLISH);
                    return("'" + c.getString(c.getColumnIndex(column)) + "'");
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress){
            // Log.d("Table oPU:", tables[progress[0]]);
            progressDialog.setMessage("Uploading table: " + tables[progress[0]]);
            double p = ((double) progress[1]/ (double) uploadColumnCount) * 100.0;
            progressDialog.setProgress((int) p);
        }

        @Override
        protected void onPostExecute(Boolean result){
            if (result == null){
                Toast t = Toast.makeText(DatabaseActionsActivity.this, "Result is Null", Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            }
            else if (result) {
                if(db.copyDatabase("archive")){
                    db.cleanDatabase("all");
                    editor.putString("lastClean", getTimestamp());
                    cleanTV.setText(String.format(res.getString(R.string.lastClean), getTimestamp()));
                    editor.putString("trip", "");
                }
                editor.putString("lastUpload", getTimestamp());
                uploadTV.setText(String.format(res.getString(R.string.lastUpload), getTimestamp()));
                editor.apply();
                makeToast("Successfully uploaded tables.");
            }
            progressDialog.dismiss();
        }
    }

    public void makeToast(String message) {
        Toast t = Toast.makeText(DatabaseActionsActivity.this, message, Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
    }

    private String getTimestamp(){
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Locale.ENGLISH);
        return s.format(new Date());
    }

    private void displayDatabaseStatus(int status){
        String message = "";
        int readRows = db.query(ReadingsDatabase.T_READING,null,null).getCount();
        int streamRows = db.query(ReadingsDatabase.T_STREAM,null,null).getCount();
        String readRowStr = "readings";
        String streamRowStr = "stream entries";

        switch(status){
            case 0:
                message = "Empty";
                break;
            case 1:
                if (readRows == 1){readRowStr = "reading";}
                message = "Initialized, " + readRows + " " + readRowStr;
                break;
            case 2:
                if (readRows == 1){readRowStr = "reading";}
                if (streamRows == 1){streamRowStr = "stream entry";}
                message = "Initialized, "
                        + readRows + " " + readRowStr + " and "
                        + streamRows + " " + streamRowStr;
                break;
            case 3:
                message = "Initialized, no readings or streaming data.";
                break;
            case 4:
                if (streamRows == 1){streamRowStr = "stream entry";}
                message = "Initialized, " + streamRows + " " + streamRowStr;
                break;
            default:
                break;
        }
        dbStatusTV.setText(String.format(res.getString(R.string.databaseStatus), message));
    }

    public void exportDatabase(View v) {
        boolean success = db.copyDatabase("");
        if (success){
            Toast.makeText(this, "Successfully copied the database", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Was not able to copy the database", Toast.LENGTH_LONG).show();
        }
    }
}