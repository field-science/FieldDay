package fieldscience.cs.earlham.edu.fieldday;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.pdf.PdfRenderer;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;
import java.util.Objects;


public class MyDocumentViewer extends AppCompatActivity {

    private File fileToOpen;
    private ImageView docImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydocviewier);
        docImageView = findViewById(R.id.myDocView);
        fileToOpen = (File) Objects.requireNonNull(getIntent().getExtras()).get("fileToOpen");

        if (Objects.requireNonNull(fileToOpen).getName().endsWith(".pdf")){
            openPDF();
            Log.d("Open PDF", fileToOpen.getName());
        } else if (fileToOpen.getName().endsWith(".jpeg") || fileToOpen.getName().endsWith("jpg") ||
                fileToOpen.getName().endsWith(".png")){
            displayImage();
            Log.d("Open JPEG", fileToOpen.getName());
        }
    }

    private void openPDF() {
        try {
            ParcelFileDescriptor fileDescriptor = ParcelFileDescriptor.open(fileToOpen, ParcelFileDescriptor.MODE_READ_ONLY);
            PdfRenderer pdfRenderer = new PdfRenderer(fileDescriptor);
            int index = 0;
            PdfRenderer.Page page = pdfRenderer.openPage(index);
            int count = pdfRenderer.getPageCount();
            Bitmap pdf = Bitmap.createBitmap(page.getWidth(), page.getHeight(), Bitmap.Config.ARGB_8888);
            page.render(pdf, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            docImageView.setImageBitmap(pdf);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void displayImage() {
        Bitmap image = BitmapFactory.decodeFile(fileToOpen.getAbsolutePath());
        docImageView.setImageBitmap(image);
    }

}
