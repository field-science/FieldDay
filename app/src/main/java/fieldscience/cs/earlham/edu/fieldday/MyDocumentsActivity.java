package fieldscience.cs.earlham.edu.fieldday;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MyDocumentsActivity extends AppCompatActivity {

    private static final String directory = Environment.getExternalStorageDirectory().toString() +
            File.separator + "FieldDay";
    private static final String TAG = "MyDocumentsActivity";
    private static List<String> downloadList;
    private static List<String> dirList;
    private static ArrayList<File> localDirList;
    private Boolean firstPage = true;
    private String baseURL = "", currentPath = "", currentDir = "", initialDir = "";
    private MyDocumentsAdapter myDocumentsAdapter;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mydocuments);

        Button downloadFiles = findViewById(R.id.downloadFileButton);
        downloadFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInToHost();
            }
        });

        downloadList = new ArrayList<>();
        dirList = new ArrayList<>();
        localDirList = new ArrayList<>();

        // Check to see if the root directory exists. If it does, get the list of files in it to
        // use for the ListView of file
        final File rootDir = createDirectory("myDocuments");
        if (Objects.requireNonNull(rootDir).listFiles() != null) {
            if (Objects.requireNonNull(rootDir.listFiles()).length != 0) {
                localDirList = getFilesInDir(rootDir);
            }
        }

        firstPage = true;
        context = this;

        ListView filesListView = findViewById(R.id.listFileResults);
        myDocumentsAdapter = new MyDocumentsAdapter(context, localDirList);
        filesListView.setAdapter(myDocumentsAdapter);
        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Objects.requireNonNull(myDocumentsAdapter.getItem(position)).isDirectory()) {
                    File folder = myDocumentsAdapter.getItem(position);
                    localDirList = getFilesInDir(Objects.requireNonNull(folder));
                    if (!Objects.requireNonNull(folder).equals(rootDir)) {
                        localDirList.add(folder.getParentFile());
                    }
                    myDocumentsAdapter.clear();
                    myDocumentsAdapter.addAll(localDirList);
                    myDocumentsAdapter.notifyDataSetChanged();
                } else {
                    Intent i = new Intent(context, MyDocumentViewer.class);
                    i.putExtra("fileToOpen", myDocumentsAdapter.getItem(position));
                    startActivity(i);
                }
            }
        });

        settings= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = settings.edit();
        editor.apply();
    }


    private void signInToHost() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(MyDocumentsActivity.this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.alert_dialog_sigin, null);
        final EditText host = dialog.findViewById(R.id.fileHost);
        host.setText(settings.getString("docURL", ""));
        final EditText path = dialog.findViewById(R.id.directoryPath);
        path.setText(settings.getString("docPath", ""));
        builder.setView(dialog);
        builder.setNegativeButton("Cancel", null);
        builder.setTitle("File Host Details");

        builder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String[] info = {host.getText().toString(), path.getText().toString()};
                editor.putString("docURL", host.getText().toString());
                editor.putString("docPath", path.getText().toString());
                editor.apply();
                new GetListOfFiles().execute(info);
            }
        });
        builder.create().show();
    }

    private ArrayList<File> getFilesInDir(File directory){
        ArrayList<File> list = new ArrayList<>();
        if (Objects.requireNonNull(directory.listFiles()).length == 0) {
            return null;
        } else {
            for (File f : Objects.requireNonNull(directory.listFiles())) {
                list.add(f);
                Log.d("File name", f.getName());
            }
            return list;
        }
    }

    private File createDirectory(String dirName) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            Log.d(TAG, "here");
            if (sd.canWrite()) {
                File baseDir = new File(directory);
                if (!baseDir.exists()){
                    baseDir.mkdir();
                    Log.d( TAG, "Created directory: " + directory);
                }
                File documentsDir = new File(directory + File.separator + dirName);
                if (!documentsDir.exists()){
                    documentsDir.mkdir();
                    Log.d(TAG, "Created directory: " + dirName);
                }
                return documentsDir;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("My Documents Activity", "Could not created directories in external storage");
        }
        return null;
    }


    private class GetListOfFiles extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(String... url){
            try {
                // We use JSoup to do the HTML parsing of the files available
                currentDir = url[1];
                if (firstPage){
                    baseURL = url[0];
                    currentPath = '/' + currentDir;
                    initialDir = currentDir;
                } else {
                    currentPath = '/' + initialDir + currentDir;
                }
                Document doc = Jsoup.connect(baseURL + currentPath).get();
                for (Element file : doc.select("tr td a")) {
                    // Makes sure the element selected is not the parent directory. We don't need to list that.
                    if (!file.text().equals("Parent Directory")) {
                        dirList.add(file.attr("href"));
                    }
                }
                // Now we display a list of files that we found to the user. The user will select which
                // files they want to download.
                MyDocumentsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(MyDocumentsActivity.this);
                            final String[] fileArray = dirList.toArray(new String[0]);
                            builder.setTitle("Select files to download");

                            // Check if this time is the first page or if the user selected a directory
                            // before and now we are showing a new directory listing. If it's not the
                            // first page, then we have to change the negative button to back where we
                            // will show the directory before that.
                            if (firstPage) {
                                builder.setNegativeButton("Cancel", null);
                                builder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new downloadListOfFiles().execute();
                                        dirList.clear();
                                    }
                                });
                            } else {
                                builder.setNegativeButton("Back", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();
                                        currentPath = currentPath.substring(0, currentPath.length() - currentDir.length());
                                        Log.d("Current Path", currentPath);
                                    }
                                });
                            }
                            final boolean[] checked = new boolean[fileArray.length];
                            for (int i=0; i<fileArray.length; i++){
                                if (downloadList.contains(fileArray[i])){
                                    checked[i] = true;
                                } else {
                                    checked[i] = false;
                                }
                            }
                            builder.setMultiChoiceItems(fileArray, checked, new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                    String file = fileArray[which];
                                    if (isChecked) {
                                        // This checks if it's a directory. If it is, then we have to
                                        // open a new dialog that has asks for which files in that
                                        // directory to download.
                                        if (file.matches("^(\\w+)(/)$")){
                                            firstPage = false;
                                            dirList.clear();
                                            File dir = createDirectory(fileArray[which]);
                                            if (!localDirList.contains(dir)){
                                                localDirList.add(dir);
                                            }
                                            new GetListOfFiles().execute(currentPath, file);
                                        } else {
                                            Log.d("On click", currentPath);
                                            if (currentPath.endsWith(currentDir)) {
                                                downloadList.add(currentDir + fileArray[which]);
                                            } else {
                                                downloadList.add(fileArray[which]);
                                            }
                                        }
                                    } else if (downloadList.contains(file)) {
                                        // The user has decided they don't want to download that
                                        // item anymore.

                                        // Check if the item clicked is a directory.
                                        if (file.matches("^(\\w+)(/)$")){
                                            // Delete any files in that subdirectory from the list.
                                            for (String item : fileArray){
                                                if (item.contains(file)){
                                                    downloadList.remove(item);
                                                }
                                            }

                                        }
                                        File dir = createDirectory(fileArray[which]);
                                        localDirList.remove(dir);
                                        downloadList.remove(fileArray[which]);
                                    }
                                }
                            });
                            builder.create().show();
                        }
                });
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
        }
    }


    private class downloadListOfFiles extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            URL fileHost;
            for (String file : downloadList) {
                if (file.matches("^(\\w+)(/)$")) {
                    File dir = createDirectory(file);
                    Log.i(TAG, "Creating directory:" + file);
                    localDirList.add(dir);
                } else {
                    String fileName;
                    File writeFile;
                    if (file.contains("/")) {
                        String[] tokens = file.split("/");
                        fileName = tokens[tokens.length-1];
                        String path = file.substring(0, file.length() - fileName.length());
                        createDirectory(path);
                        Log.d("FileName", fileName);
                        Log.d("Path", path);
                        writeFile = new File(directory + '/' + path, fileName);
                    } else {
                        writeFile = new File(directory, file);
                        localDirList.add(writeFile);
                    }
                    try {
                        fileHost = new URL(baseURL + '/' + initialDir + file);
                        Log.d(TAG, "Made URL for: " + baseURL + '/' + initialDir + file);
                        HttpURLConnection conn = (HttpURLConnection) fileHost.openConnection();
                        conn.setRequestMethod("GET");
                        conn.connect();
                        InputStream is = conn.getInputStream();
                        if (is != null) {
                            BufferedInputStream inputStream = new BufferedInputStream(is);
                            FileOutputStream fileOutputStream = new FileOutputStream(writeFile);
                            int totalSize = conn.getContentLength();
                            byte[] buffer = new byte[1024 * 1024];
                            int bufferLength;
                            while ((bufferLength = inputStream.read(buffer)) > 0) {
                                fileOutputStream.write(buffer, 0, bufferLength);
                            }
                            fileOutputStream.close();
                        }
                    } catch(IOException e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result){
            myDocumentsAdapter.notifyDataSetChanged();
        }
    }
}
