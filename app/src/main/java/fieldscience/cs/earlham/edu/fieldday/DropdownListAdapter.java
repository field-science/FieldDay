package fieldscience.cs.earlham.edu.fieldday;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class DropdownListAdapter extends BaseAdapter {
    private final ArrayList<Pair<String, String>> dropdownList;
    private final Context context;

    public DropdownListAdapter(Context context) {
        super();
        dropdownList = new ArrayList<>();
        this.context = context;
    }

    public void addItems(List<Pair<String, String>> items) {
        dropdownList.addAll(items);
    }

    public void addItem(Pair<String, String> item) {
        dropdownList.add(item);
    }

    public int getIndexOf(Pair<String, String> item){
        return dropdownList.indexOf(item);
    }

    @Override
    public int getCount(){
        return dropdownList.size();
    }

    @Override
    public Object getItem(int position){
        return dropdownList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clear() {
        dropdownList.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolderItem holder;
        Pair<String, String> item = (Pair<String, String>) getItem(position);
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inflater).inflate(R.layout.dropdown_item, parent, false);
            holder = new ViewHolderItem();
            holder.itemName = view.findViewById(R.id.itemName);
            view.setTag(holder);

        } else {
            holder = (ViewHolderItem) view.getTag();
        }

        holder.itemName.setText(item.first); // the String
        return view;
    }

    class ViewHolderItem {
        TextView itemName;

        ViewHolderItem() {
            // empty constructor
        }
    }


}
