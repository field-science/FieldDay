package fieldscience.cs.earlham.edu.fieldday;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

class MyDocumentsAdapter extends ArrayAdapter<File> {

    private final Context context;

    public MyDocumentsAdapter(Context c, ArrayList<File> files) {
        super(c, R.layout.file_list, files);
        context = c;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        File file = getItem(position);

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Objects.requireNonNull(layoutInflater).inflate(R.layout.file_list, parent, false);
        }

        TextView fName = convertView.findViewById(R.id.objectName);
        ImageView fType = convertView.findViewById(R.id.objectType);


        fName.setText(Objects.requireNonNull(file).getName());
        if (file.isDirectory()){
            fType.setImageDrawable(context.getResources().getDrawable(R.drawable.folder_black_icon, null));
        } else {
            fType.setImageDrawable(context.getResources().getDrawable(R.drawable.file_white_icon, null));
        }

        return convertView;
    }




}
