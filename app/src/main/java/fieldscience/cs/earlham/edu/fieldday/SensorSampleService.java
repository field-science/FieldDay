package fieldscience.cs.earlham.edu.fieldday;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class SensorSampleService extends Service implements SensorEventListener {

    private BluetoothLeService btService;
    private String sensorType, response;
    private boolean sampleOnce;
    private boolean registered = false;
    private boolean streaming = false;
    private boolean calibrating = false;
    private boolean connected = false;
    private long loggingInterval;
    private Handler dbHandler, btHandler;
    private Runnable dbRunner, btRunNames;
    private BluetoothSensor bluetoothSensor;
    private ArrayList<aSensor> sensorList;
    private ReadingsDatabase db;
    private SensorManager mSensorManager;
    private ArrayList<Sensor> builtinList;
    private static double lastLongitude;
    private static double lastLatitude;
    private static double lastElevation;
    private static int lastSatellites;
    private static float lastAccuracy;
    private static String tripName;
    private static String site;
    private static String sector;
    private static String spot;
    private static String pictureName;
    private static String sampleNotes;
    private PowerManager.WakeLock mWakeLock;

    // These are used in the Intent that SensorSampleActivity creates to start the Service.
    public static final String SENSOR_TYPE = "Sensor Type";
    public static final String LOG_INTERVAL = "Logging Interval";
    public static final String SAMPLE_ONCE = "Sample or Streaming";
    public static final String SENSOR_LIST = "List of Sensors";
    public static final String TRIP_NAME = "Name of Trip";
    public static final String SITE = "Name of Site";
    public static final String SECTOR = "Name of Sector";
    public static final String SPOT = "Spot Number";
    public static final String BT_SENSOR = "Bluetooth Sensor";

    public static final String ACTION_CONNECTED = "Connected.";
    public static final String ACTION_UPDATED_LIST = "Updated Sensor List.";
    public static final String ACTION_DISCONNECTED = "Disconnect";

    private static final String TAG = "SensorSampleService";

    private final ServiceConnection btServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            btService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!btService.initialize()) {
                Log.e(TAG, "Bluetooth Stopped Running");
                stopSelf();
            }
            btService.connect(bluetoothSensor);
            Log.d(TAG, "Bound to Bluetooth Service");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            btService = null;
            dbHandler.removeCallbacks(dbRunner);
//            if (sensorType.equals("bluetooth")) {
//                //btHandler.removeCallbacks(btRunValues);
//            }
        }
    };


    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                btService = null;
                dbHandler.removeCallbacksAndMessages(dbRunner);
                btHandler.removeCallbacksAndMessages(btRunNames);
                connected = false;
                //btHandler.removeCallbacksAndMessages(btRunValues);
                broadcastUpdate(ACTION_DISCONNECTED);
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                btService.readMessage();
                if (!registered) {
                    btHandler.postDelayed(btRunNames, 5000);
                    btHandler.removeCallbacksAndMessages(btRunNames);
                    boolean clear = false;
                } else {
                    getSensorValues();
                }
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                if (btService != null) {
                    if (BluetoothLeService.current_request.equals(BluetoothLeService.LIST_OF_SENSORS)) {
                        parseResponse(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                    } else if (BluetoothLeService.current_request.equals(BluetoothLeService.SENSOR_VALUES)) {
                        parseResponse(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                    }
                }
            }
        }
    };

    // Broadcasting updated to SensorSampleActivity
    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        intent.putParcelableArrayListExtra(SENSOR_LIST, sensorList);
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        public SensorSampleService getService() {
            return SensorSampleService.this;
        }
    }

    @Override
    public void onCreate() {
        btHandler = new Handler();
        btRunNames = new Runnable() {
            @Override
            public void run() {
                getSensorNames();
            }
        };
        //btRunValues = new Runnable() {
        //    @Override
        //    public void run() {
        //        getSensorValues();
        //        btHandler.postDelayed(this, 5000);
        //   }
        //};

        dbHandler = new Handler();
        dbRunner = new Runnable() {
            @Override
            public void run() {
                writeToDB();
                dbHandler.postDelayed(this, loggingInterval);
            }
        };

        response = "";
        pictureName = "";
        sampleNotes = "";
        loggingInterval = 5000;

        sensorList = new ArrayList<>();
        builtinList = new ArrayList<>();

        db = ReadingsDatabase.getInstance(this);
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new myLocationListener();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        setLocation(Objects.requireNonNull(locationManager).getLastKnownLocation(LocationManager.GPS_PROVIDER));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 1, locationListener);
    }

    class myLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            setLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    }

    private void setLocation(Location location) {
        if (location != null) {
            lastLongitude = location.getLongitude();
            lastLatitude = location.getLatitude();
            lastElevation = location.getAltitude();
            lastAccuracy = location.getAccuracy();
            lastSatellites = location.getExtras().getInt("satellites");
        }
    }

    // This is called when another activity or component called startService with the class as the
    // intent
    @SuppressLint("WakelockTimeout")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d(TAG, "Starting service.");

        // Set the sensor type and the logging interval from the activity that started the service
        loggingInterval = Objects.requireNonNull(intent.getExtras()).getLong(LOG_INTERVAL);
        sampleOnce = intent.getExtras().getBoolean(SAMPLE_ONCE);
        tripName = intent.getExtras().getString(TRIP_NAME);
        site = intent.getExtras().getString(SITE);
        sector = intent.getExtras().getString(SECTOR);
        spot = intent.getExtras().getString(SPOT);
        dbHandler = new Handler();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = Objects.requireNonNull(pm).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "fieldday:samplingwakelock");
        mWakeLock.acquire();

        if (sampleOnce) {
            Log.d(TAG, "writeToDB.");
            writeToDB();
        } else {
            dbHandler.postDelayed(dbRunner, loggingInterval);
            streaming = true;
        }

        // START_STICKY means that the service continues running until something outside tells it
        // to stop. START_NOT_STICKY the service would stop when there's no more work to do.
        return Service.START_STICKY;
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

   @Override
    public IBinder onBind(Intent intent) { return mBinder; }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        if (btService != null) {
            unregisterReceiver(mGattUpdateReceiver);
            btService.disconnect();
            btHandler.removeCallbacksAndMessages(btRunNames);
            //btHandler.removeMessages(0);
            //btHandler.removeCallbacksAndMessages(btRunValues);
            btService.close();
            connected = false;
            unbindService(btServiceConnection);
        }
        if (dbHandler != null) {
            dbHandler.removeCallbacksAndMessages(dbRunner);
        }
        for (Sensor s : builtinList) {
            mSensorManager.unregisterListener(this, s);
        }
    }

    public void stopSampling() {
        dbHandler.removeMessages(0);
        dbHandler.removeCallbacksAndMessages(dbRunner);
        dbHandler = null;
        streaming = false;
        mWakeLock.release();
    }

    private final IBinder mBinder = new LocalBinder();

    private void parseResponse(String d) {
        response += d;
        Log.d(TAG, response);
        if (response.endsWith("done")) {
            if (BluetoothLeService.current_request.equals(BluetoothLeService.LIST_OF_SENSORS)) {
                updateSensorList();
            } else {
                updateSensorValues();
                response = "";
            }
        } else if (response.equals("calibrated")) {
            Toast t = Toast.makeText(SensorSampleService.this, "Calibrated!", Toast.LENGTH_SHORT);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            calibrating = false;
            getSensorValues();
            response = "";
        }
    }

    private void updateSensorList() {
        StringBuilder sensor = new StringBuilder();
        if (!registered) {
            for (int i = 0; i < response.length(); i++) {
                char c = response.charAt(i);
                if (c == ';') {
                    String delims = "[,]";
                    String[] tokens = sensor.toString().split(delims);
                    aSensor sens = new aSensor(tokens[1], tokens[0]);
                    sens.setPlatform(bluetoothSensor.getDeviceName());
                    sensorList.add(sens);
                    sensor = new StringBuilder();
                } else {
                    sensor.append(c);
                }
            }
            btHandler.removeMessages(0);
            btHandler.removeCallbacksAndMessages(btRunNames);
            //btHandler.postDelayed(btRunValues, 3000);
            //getSensorThread.start();
            getSensorValues();
        }
        response = "";
    }

    private void updateSensorValues() {
        StringBuilder sensor = new StringBuilder();
        for (int i = 0; i < response.length(); i++) {
            char c = response.charAt(i);
            if (c == ';') {
                String delims = "[,]";
                String[] tokens = sensor.toString().split(delims);
                aSensor sens = getSensor(tokens[1], null);
                float[] value = {0, 0.0f, 0.0f};
                value[0] = Float.parseFloat(tokens[2]);
                Objects.requireNonNull(sens).setLastValue(value, 1);
                sens.setLastValueQuality(0);
                Log.d(TAG, "Sensor Value: " + sens.getLastValueString());
                Log.d(TAG, "Sensor Name: " + sens.getName());
                sensor = new StringBuilder();
            } else {
                sensor.append(c);
            }
        }
        if (registered) {
            broadcastUpdate(ACTION_UPDATED_LIST);
        } else {
            registered = true;
            connected = true;
            broadcastUpdate(ACTION_CONNECTED);
        }
        response = "";
        if (!calibrating){
            if (connected) {
                getSensorValues();
            }
        }
    }

    public void connectSensor(String sensor, BluetoothSensor btSensor) {
        sensorType = sensor;
        if (sensor.equals("bluetooth")){
            bluetoothSensor = btSensor;
            Intent i = new Intent(this, BluetoothLeService.class);
            bindService(i, btServiceConnection, Context.BIND_AUTO_CREATE);
            registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
            Log.d(TAG, "Starting Bluetooth Service");
        } else {
            // xxx Following is messy code that ought to change.
            // We only want to see ~4 or 5 of these built in sensors - the rest are much less useful
            // First we fetch all the device's sensors to the builtinList, then we iterate thru
            // builtinList, register listeners for the sensors we care about, and make a list of
            // sensors to remove from builtinList. There is likely a much more elegant/efficient
            // way to do this, but I know too little about Java List vs ArrayList stuff to do it - Eli

            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            Log.d(TAG, "Getting built in sensor information...");
            builtinList.addAll(mSensorManager.getSensorList(Sensor.TYPE_ALL));
            Log.d(TAG, "Built in sensors: " + builtinList.toString());

            List<Sensor> toRemove = new ArrayList<>();

            for (Sensor s : builtinList){
                if (s.getStringType().contains("android")){
                    if (!s.getName().contains("Secondary")){
                        // Following are sensors we care about; magnetic field & pressure for science
                        // Accelerometer, light and step counter for easy verification/fun
                        if ((s.getType() == Sensor.TYPE_MAGNETIC_FIELD ) ||
                            (s.getType() == Sensor.TYPE_LIGHT ) ||
                            (s.getType() == Sensor.TYPE_PRESSURE ) ||
                            (s.getType() == Sensor.TYPE_ACCELEROMETER ) ||
                            (s.getType() == Sensor.TYPE_STEP_COUNTER )) {
                            mSensorManager.registerListener(this, s, 2000000);
                        } else {
                            toRemove.add(s);
                        }
                    }
                }
            }

            builtinList.removeAll(toRemove);

            for (int i=0; i<builtinList.size(); i++) {
                // This is to figure out which sensors are uncalibrated
                String delims = "[ .]+";
                if (builtinList.get(i).getStringType().startsWith("android")){
                    if (!builtinList.get(i).getName().contains("Secondary")) {
                            aSensor sens = new aSensor(builtinList.get(i));
                            String[] tokens = sens.getStringType().split(delims);
                            sens.setName(tokens[2]);
                            sensorList.add(sens);
                    }
                }
            }
        }
    }

    public void setPictureName(String name) {
        pictureName = name;
    }

    public void setSampleNotes(String notes) { sampleNotes = notes; }

    private void getSensorNames() {
        String getSensors = "00";
        Log.d(TAG, "Writing message for sensor names.");
        btService.writeMessage(getSensors, BluetoothLeService.LIST_OF_SENSORS);
        btHandler.removeCallbacksAndMessages(btRunNames);
    }

    public void onSpotChanged(String newSpot) {
        spot = newSpot;
    }

    public void onSectorChanged(String newSector) {
        sector = newSector;
    }

    public void onSiteChanged(String newSite) {
        site = newSite;
    }

    public void onLoggingTimeChanged(long newLoggingInterval) {
        loggingInterval = newLoggingInterval;
        if (streaming) {
            dbHandler.removeCallbacksAndMessages(dbRunner);
            dbHandler.postDelayed(dbRunner, loggingInterval);
        }
    }

    private aSensor getSensor(String sensorID, Sensor sensor) {
        for (aSensor s : sensorList) {
            if (sensorType.equals("bluetooth")) {
                String aSensorID = s.getID();
                if (sensorID.equals(aSensorID)) {
                    return s;
                }
            } else {
                String sensorType = sensor.getStringType();
                String aSensorType = s.getStringType();
                if (sensorType.equals(aSensorType)) {
                    return s;
                }
            }
        }
        return null;
    }

    private void writeToDB() {
        double[] geoInfo = {lastLatitude, lastLongitude, lastElevation};
        boolean success = false;
        float[] lastValues;
        String message = "";
        // The user has connected and downloaded data from a remote database
        for (aSensor sensor : sensorList) {
            lastValues = sensor.getLastValues();
            if (lastValues != null) {
                String table;
                if (!sampleOnce) {
                    table = "fieldday_streaming";

                    db.addReading(sensor, lastValues, sensor.getLastValueQuality(), getTimestamp(),
                            site, sector, spot, geoInfo, tripName, lastAccuracy, lastSatellites, sampleNotes, "", table);
                    Log.d(TAG, "Successfully added to streaming");
                } else {
                    table = "fieldday_reading";

                    Log.d(TAG, sensor.getName() +"lastValues: " + Arrays.toString(lastValues) + " sensor.getLastValueQuality(): "+ sensor.getLastValueQuality() + " " + getTimestamp() + " " + site + " " + sector + " " + spot);
                    success = db.addReading(sensor, lastValues, sensor.getLastValueQuality(), getTimestamp(),
                            site, sector, spot, geoInfo, tripName, lastAccuracy, lastSatellites, sampleNotes, pictureName, table);
                    if (success) {
                        message = "Successfully took a sample";
                    } else {
                        message = "Could not write to the database. Try again.";
                    }
                    stopSelf();
                }
            }
            if (success) {
                Toast t = Toast.makeText(SensorSampleService.this, message, Toast.LENGTH_SHORT);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                //btHandler.postDelayed(btRunValues, loggingInterval);
                //getSensorValues();
            }
        }
        pictureName = "";
    }

    private String getTimestamp(){
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Locale.ENGLISH);
        return s.format(new Date());
    }

    private void getSensorValues() {
        String getSensors = "vals";
        btService.writeMessage(getSensors, BluetoothLeService.SENSOR_VALUES);
        Log.d(TAG, "Retrieving Sensor Values");
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        aSensor sens = getSensor(null, sensor);
        float[] values = event.values;
        //Log.d(TAG, Integer.toString(event.values.length));
        switch (sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and1");
                break;
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and2");
                break;
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and3");
                break;
            case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and4");
                break;
            case Sensor.TYPE_GRAVITY:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and5");
                break;
            case Sensor.TYPE_GYROSCOPE:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and6");
                break;
            case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and7");
                break;
            case Sensor.TYPE_HEART_RATE:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and8");
                break;
            case Sensor.TYPE_LIGHT:
                Objects.requireNonNull(sens).setLastValue(values, 1);
                sens.setID("and9");
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and10");
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and11");
                break;
            case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and12");
                break;
            case Sensor.TYPE_PRESSURE:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and13");
                break;
            case Sensor.TYPE_PROXIMITY:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and14");
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and15");
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and16");
                break;
            case Sensor.TYPE_SIGNIFICANT_MOTION:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and17");
                break;
            case Sensor.TYPE_STEP_COUNTER:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and18");
                break;
            case Sensor.TYPE_STEP_DETECTOR:
                Objects.requireNonNull(sens).setLastValue(values, values.length);
                sens.setID("and19");
                break;
        }
        Objects.requireNonNull(sens).setLastValueQuality(0);
        broadcastUpdate(ACTION_UPDATED_LIST);
    }
    public void calibrateSensor(Float value, String sensor) {
        //btHandler.removeCallbacksAndMessages(btRunValues);
        //btHandler.removeMessages(0);
        //btHandler.removeCallbacksAndMessages(btRunValues;

        String sensorid = "";
        for (aSensor s : sensorList) {
            if (s.getName().equals(sensor)) {
                sensorid = s.getID();
            }
        }

        calibrating = true;
        String calString = "cal;"+sensorid+";"+value.toString();
        btService.writeMessage(calString, BluetoothLeService.SENSOR_VALUES);
        Log.d(TAG, "calibrating");
        Log.d(TAG, calString);
    }

    public void controlCalibrating(boolean control) {
        calibrating = control;
        if (!control) {
            getSensorValues();
        }
    }
}
