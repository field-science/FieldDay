package fieldscience.cs.earlham.edu.fieldday;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class BluetoothSensor implements Parcelable {

    private List<UUID> uuids;
    private final String address;
    private final String name;
    private final BluetoothDevice device;
    private final GattClient gattClient;
    private Listener mListener;
    private BluetoothGattCharacteristic readChar, writeChar;

    public BluetoothSensor(BluetoothDevice device){
        Log.d("Bluetooth Sensor", device.toString());
        this.device = device;
        address = device.getAddress();
        name = device.getName();
        gattClient = new GattClient();
    }

    // Get bluetooth device's name
    public String getDeviceName(){
        return name;
    }

    // Get bluetooth device's address
    public String getAddress() {
        return address;
    }

    // Get a list of all available services on bluetooth device
    public List<BluetoothGattService> getServices(){
        return gattClient.getServices();
    }

    public BluetoothGattService getService(UUID service){
        return gattClient.getService(service);
    }

    // Write a value to one of the services characteristics
    public void writeMessage(byte[] tx){
        writeChar.setValue(tx);
        gattClient.writeCharacteristic(writeChar);
    }

    // Read a message from one of the services characteristics
    public void readMessage(){
        gattClient.readCharacteristic(readChar);
    }

    //public void setPrimaryService(BluetoothGattService service){ }

    public void setCharacteristics(BluetoothGattCharacteristic writeChar, BluetoothGattCharacteristic readChar){
        this.writeChar = writeChar;
        this.readChar = readChar;
    }

    // Set to be notified for a characteristic
    public void setToNotify(BluetoothGattCharacteristic characteristic, boolean enabled){
        gattClient.setCharacteristicNotification(characteristic, enabled);
    }

    // Returns true if connection succeeded
    public void connect(Context context){
        gattClient.connect(context, device);

        // This listener is used when changes happen in the GattClient class. It is implemented in
        // the GattClient class and anytime these functions are called in that class, the listener in
        // this class are called as well. The GattClient class is used for communication with the
        // actual bluetooth device. This is used for talking to the BluetoothService and
        // SensorSampleService.
        GattClient.Listener listener = new GattClient.Listener() {
            @Override
            public void onMessageReceived(byte[] data, BluetoothGattCharacteristic characteristic) {
                Log.d("Bluetooth Sensor", "On characteristic read");
                StringBuilder d = new StringBuilder();
                try {
                    for (byte b : data) {
                        d.append((char) b);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                Log.d("Bluetooth Sensor", "Data:" + d);
                mListener.onCharacteristicRead(characteristic.getUuid(), d.toString());
            }

            @Override
            public void onServicesDiscovered() {
                mListener.onServiceInformation();
            }
        };
        gattClient.setListener(listener);
        gattClient.isConnected();
    }

    // Disconnects from the GATT server
    public void disconnect() {
        gattClient.disconnect();
    }

    public void setListener(Listener listener){
        this.mListener = listener;
    }

    public interface Listener {
        void onCharacteristicRead(UUID characteristicUUID, String data);
        void onServiceInformation();
    }

    // Required by Android Parcelable class
    @Override
    public int describeContents() {
        return 0;
    }

    // Required by Android Parcelable class
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(device, 0);
    }

    public static final Parcelable.Creator<BluetoothSensor> CREATOR = new Parcelable.Creator<BluetoothSensor>() {
        public BluetoothSensor createFromParcel(Parcel in) {
            BluetoothDevice bt = in.readParcelable(((Object) this).getClass().getClassLoader());
            return new BluetoothSensor(Objects.requireNonNull(bt));
        }
        public BluetoothSensor[] newArray(int size) {
            return new BluetoothSensor[size];
        }
    };

}
