package fieldscience.cs.earlham.edu.fieldday;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class MainScreenActivity extends AppCompatActivity {
    private FragmentManager fragman;
    private Fragment frag;
    private String tripName;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainScreenActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainscreen_frame);
        context = this;

        fragman = getSupportFragmentManager();
        frag = fragman.findFragmentById(R.id.mainButtonContainer);

        // Check if fragment is null, and if it is, create a MainScreen fragment
        // which is the default fragment
        if (frag == null) {
            frag = new MainScreenFragment();
            fragman.beginTransaction()
                    .add(R.id.mainButtonContainer, frag)
                    .commit();
        }
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        tripName = settings.getString("trip", "");
    }

    public void activityStart(View view){
        switch(view.getId()){
            case R.id.buttonAbout:
                frag = new AboutFragment();
                break;
            case R.id.buttonSettings:
                frag = new SettingsFragment();
                break;
            //case R.id.imageButtonTakeSample:
            //    frag = new SampleButtonsFragment();
            //    break;
            case R.id.buttonNotebook:
                frag = new LabNotebookFragment();
                break;
            default:
                frag = null;
        }
        fragman.beginTransaction()
                .replace(R.id.mainButtonContainer, frag)
                .addToBackStack(null)
                .commit();
    }

    public void startSampleActivity(View v) {
        Intent i;
        switch(v.getId()){
            case R.id.imageButtonDbActions:
                i = new Intent(context, DatabaseActionsActivity.class);
                startActivity(i);
                break;
            case R.id.imageButtonBluetooth:
                i = new Intent(context, SensorScanActivity.class);
                checkDatabase(i);
                break;
            case R.id.imageButtonBuiltIn:
                i = new Intent(context, SensorSampleActivity.class);
                i.putExtra(SensorSampleActivity.WHICH_SENSOR, "built-in");
                checkDatabase(i);
                break;
            case R.id.imageButtonBenchSample:
                i = new Intent(context, BenchSampleActivity.class);
                checkDatabase(i);
                break;
        }
    }

    public void labNotebookStart(View v){
        Intent i;
        switch(v.getId()){
            case R.id.imageButtonDrive:
                i = new Intent(getPackageManager().getLaunchIntentForPackage("com.google.android.apps.docs"));
                startActivity(i);
                break;
            case R.id.imageButtonNotePad:
                i = new Intent(context, MyDocumentsActivity.class);
                startActivity(i);
                break;
        }
    }

    private void checkDatabase(final Intent intent) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        tripName = settings.getString("trip", "");
        if (tripName.equals("")) {
            Dialog d = new AlertDialog.Builder(MainScreenActivity.this)
                    .setTitle("No Remote Database Connection")
                    .setMessage("You have not setup your local database using a remote server. You won't be able " +
                            "to record any data, are you sure you want to continue?")
                    .setNegativeButton("Connect remote database server", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(context, DatabaseActionsActivity.class);
                            startActivity(i);

                        }
                    })
                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            intent.putExtra(SensorSampleActivity.REMOTE_DB, false);
                            startActivity(intent);
                        }
                    })
                    .create();
            d.show();
        } else {
            Log.d("DATABASE TRIP",tripName);
            intent.putExtra(SensorSampleActivity.REMOTE_DB, true);
            startActivity(intent);
        }
    }
}
