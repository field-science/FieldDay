package fieldscience.cs.earlham.edu.fieldday;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.preference.PreferenceManager;

import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class BenchSampleActivity extends AppCompatActivity {

    private static final String TAG = BenchSampleActivity.class.getSimpleName();
    private static final String REMOTE_DB = "Remote database onnected";
    private static final String table = "fieldday_reading";

    private Spinner sectorSpinner;
    private Spinner spotSpinner;
    private ReadingsDatabase db;
    private String site;
    private String sector;
    private String spot;
    private String platform;
    private String sensorName;
    private String sensorValue;
    private String sensorQuality;
    private String tripName;
    private String readingNotes;
    private String pictureName;
    private List<String> spotList;
    private Pair<String, String> sitePair;
    private Pair<String, String> sectorPair;
    private Pair<String, String> sensorPair;
    private DropdownListAdapter siteAdapter, sectorAdapter, sensorAdapter;
    private Context context;
    private ProgressDialog progress;
    private static final String directory = ReadingsDatabase.directory;
    private static final aSensor sensor = new aSensor("someid","somename");
    private Uri pictureUri;
    private File baseDir;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_benchsample);

        context = this;
        db = ReadingsDatabase.getInstance(this);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        tripName = settings.getString("trip", "");
        Log.d("BenchSampleActivity", "tripName: "+tripName);
        TextView trip_name = findViewById(R.id.tripNameText);
        trip_name.setText(String.format("Trip Name: %s", tripName));

        List<Pair<String, String>> siteList = new ArrayList<>();
        List<Pair<String, String>> sectorList = new ArrayList<>();
        spotList = new ArrayList<>();
        List<String> platformList = new ArrayList<>();
        List<Pair<String, String>> sensorList = new ArrayList<>();

        boolean remote_db = getIntent().getBooleanExtra(REMOTE_DB, false);
        readingNotes = "";
        Button recordButton = findViewById(R.id.recordButton);
        Button addPictureButton = findViewById(R.id.addPicture);
        baseDir = new File(directory);
        if (!baseDir.exists()){
            baseDir.mkdir();
        }

        // Site spinner, site adapter
        if (remote_db) { siteList = db.getSites(null); }
        siteList.add(0, new Pair<>("Site", "0"));
        Spinner siteSpinner = findViewById(R.id.selectSite);
        siteAdapter = new DropdownListAdapter(this);
        siteAdapter.addItems(siteList);
        siteSpinner.setAdapter(siteAdapter);

        // Sector spinner, sector adapter
        sectorSpinner = findViewById(R.id.selectSector);

        // Spot spinner, spot adapter
        spotSpinner = findViewById(R.id.selectSpot);

        // Platform spinner, platform adapter
        if (remote_db) { platformList.addAll(db.getPlatforms()); }
        platformList.add(0, "Platform");
        Spinner platformSpinner = findViewById(R.id.selectPlatform);
        ArrayAdapter<String> platformAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, platformList);
        platformAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        platformSpinner.setAdapter(platformAdapter);

        // Sensor spinner, sensor adapter
        sensorList.add(0, new Pair<>("Sensor", "0"));
        Spinner sensorSpinner = findViewById(R.id.selectSensor);
        sensorAdapter = new DropdownListAdapter(this);
        sensorAdapter.addItems(sensorList);
        sensorSpinner.setAdapter(sensorAdapter);

        siteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    sitePair = (Pair<String, String>) siteAdapter.getItem(position);
                    site = sitePair.first;
                    sector = null;
                    sectorAdapter = new DropdownListAdapter(context);
                    sectorAdapter.addItems(db.getSectors(site));
                    sectorSpinner.setAdapter(sectorAdapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sectorPair = (Pair<String, String>) sectorAdapter.getItem(position);
                sector = sectorPair.first;
                spot = null;
                spotList = db.getSpots(site,sector);
                final ArrayAdapter<String> spotAdapter = new ArrayAdapter<>(context,
                        android.R.layout.simple_spinner_item, spotList);
                spotAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spotSpinner.setAdapter(spotAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spotSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spot = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        platformSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    platform = parent.getItemAtPosition(position).toString();
                    sensorName = null;
                    sensorAdapter.clear();
                    sensorAdapter.addItem(new Pair<>("Sensor", "0"));
                    sensorAdapter.addItems(db.getSensors(platform));
                    sensorAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sensorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    sensorPair = (Pair<String,String>) sensorAdapter.getItem(position);
                    sensorName = sensorPair.first;
                    sensor.setID(sensorPair.second);
                    sensor.setName(sensorPair.first);
                    sensor.setPlatform(platform);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        EditText notesET = findViewById(R.id.readingNotes);
        notesET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                readingNotes = s.toString();
            }
        });

        EditText valueET = findViewById(R.id.sensorValue);
        valueET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                sensorValue = s.toString();
                Log.d(TAG, "Sensor Value: " + sensorValue);
                if (!sensorValue.equals("")) {
                    sensor.setLastValue(new float[] {Float.parseFloat(sensorValue), 0.0f, 0.0f}, 1);
                }
            }
        });

        EditText qualityET = findViewById(R.id.sensorQuality);
        qualityET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                sensorQuality = s.toString();
                if (!sensorQuality.equals("")) {
                    sensor.setLastValueQuality(Float.parseFloat(sensorQuality));
                }
            }
        });
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFields()) {
                    recordSample();
                } else {
                    makeToast("Make sure you've entered or selected all of the appropriate information" +
                            " for this sample.");
                }
            }
        });
    }

    private boolean checkFields() {
        if ((site == null) || (sector == null) || (spot == null) || (platform == null) ||
                (sensorQuality == null)) {
            return false;
        } else {
            return true;
        }
    }

    private void makeToast(String message) {
        Toast t = Toast.makeText(this, message, Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
        t.show();
    }

    private void recordSample() {
        Map<String, Float> geo;
        geo = findGeoInfo();
        if (! geo.isEmpty()) {
            double[] geoInfo = new double[] {
                    geo.get("latitude"), geo.get("longitude"), geo.get("elevation")
            };

            boolean success = db.addReading(sensor, sensor.getLastValues(), sensor.getLastValueQuality(), getTimestamp(),
                    site, sector, spot, geoInfo, tripName, geo.get("accuracy"), Math.round(geo.get("satellites")), readingNotes, pictureName, table);
            if (success) {
                makeToast("success!");
            } else {
                makeToast("not successful");
            }
        }//insert a new entry to the table

    }

    private String getTimestamp() {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Locale.ENGLISH);
        return s.format(new Date());
    }

    private Map<String, Float> findGeoInfo() {
        Map<String,Float> geo = new HashMap<>();
        Cursor c = db.query(ReadingsDatabase.T_READING,
                "siteID = ? and sectorID = ? and spotID = ?",
                new String[]{//site, sector, spot}
                      ((Pair<String, String>) (siteAdapter.getItem(siteAdapter.getIndexOf(sitePair)))).second,
                      ((Pair<String, String>) (sectorAdapter.getItem(sectorAdapter.getIndexOf(sectorPair)))).second,
                      spot
                }
        );
        //c.moveToFirst(); //move inside the following else statement
        if (c.getCount() == 0) {
            makeToast("Unfortunately, I can't find a latitude and longitude for this trip, site, sector and spot you selected.");
            Log.d("BenchSample_noResult", "Site:" + ((Pair<String, String>) (siteAdapter.getItem(siteAdapter.getIndexOf(sitePair)))).second);
            Log.d("BenchSample_noResult", "Sector:"+((Pair<String, String>) (sectorAdapter.getItem(sectorAdapter.getIndexOf(sectorPair)))).second);
            Log.d("BenchSample_noResult", "Spot: "+spot);

            //get the current site's latitude, longitude and elevations
            //geo.put("latitude", );


        } else {
            c.moveToFirst();
            geo.put("latitude", c.getFloat(c.getColumnIndex("latitude")));
            geo.put("longitude", c.getFloat(c.getColumnIndex("longitude")));
            geo.put("elevation", c.getFloat(c.getColumnIndex("elevation")));
            geo.put("accuracy", c.getFloat(c.getColumnIndex("accuracy")));
            geo.put("satellites", (float) (c.getInt(c.getColumnIndex("satellites"))));
            Log.d("BenchSample_data", "latitude: "+geo.get("latitude"));
            Log.d("BenchSample_data", "longitude: "+geo.get("longitude"));
            Log.d("BenchSample_data", "elevation: "+geo.get("elevation"));
            Log.d("BenchSample_data", "accuracy: "+geo.get("accuracy"));
            Log.d("BenchSample_data", "satellites: "+geo.get("satellites"));
        }
        c.close();
        return geo;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        final View dialog = layoutInflater.inflate(R.layout.alert_picture_name, null);
        final EditText name = dialog.findViewById(R.id.pictureName);

        if (resultCode != Activity.RESULT_CANCELED) {
            Dialog d = new AlertDialog.Builder(BenchSampleActivity.this)
                    .setTitle("Picture Name")
                    .setView(dialog)
                    .setNegativeButton("Discard Picture", null)
                    .setPositiveButton("Save Picture", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pictureName = name.getText().toString();
                            try {
                                dialog.dismiss();
                                new saveImage().execute(android.provider.MediaStore.Images.Media.getBitmap(
                                                context.getContentResolver(), pictureUri));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    })
                    .show();
        }
    }

    public void takePicture(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(BenchSampleActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    1);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(BenchSampleActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(BenchSampleActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            return;
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        File tempFile = null;
        try {
            tempFile = createTempFile();
            tempFile.delete();
        } catch (Exception e){
            e.printStackTrace();
        }
        pictureUri = Uri.fromFile(tempFile);
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
        startActivityForResult(i, 0);
    }

    // By default android pictures using the Media.IMAGE_CAPTURE are not full size when returning from
    // the activity. I crete a temporary directory to save the file in and retrieve it later.
    private File createTempFile() throws Exception {
        File temp = new File(baseDir, "tmp");
        if (!temp.exists()){
            temp.mkdir();
        }
        return File.createTempFile("picture", ".png", temp);
    }

    private class saveImage extends AsyncTask<Bitmap,Void,Boolean> {
        Bitmap b = null;

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(context);
            progress.setMessage("Please wait. Saving photo.");
            progress.setCancelable(false);
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
        }

        @Override
        protected Boolean doInBackground(Bitmap... params){
            b = params[0];
            try {
                File sd = Environment.getExternalStorageDirectory();

                if (sd.canWrite()) {
                    File baseDir = new File(directory);
                    if (!baseDir.exists()){
                        baseDir.mkdir();
                    }
                    File picturesDir = new File(baseDir, "pictures");
                    if (!picturesDir.exists()) {
                        picturesDir.mkdir();
                    }
                    int count = 0;
                    for (File f : Objects.requireNonNull(picturesDir.listFiles())){
                        if (f.getName().startsWith(pictureName)){
                            count += 1;
                        }
                    }

                    File picture = new File(picturesDir, pictureName + ".png");
                    if (picture.exists()) {
                        if (count > 0) {
                            picture = new File(picturesDir, pictureName + "-" + count + ".png");
                        }
                    }

                    FileOutputStream fOut = new FileOutputStream(picture);
                    b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();

                    MediaStore.Images.Media.insertImage(context.getContentResolver(),
                            picture.getAbsolutePath(), picture.getName(), picture.getName());
                    pictureName = picture.getName();
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return null;
        }

        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast t = Toast.makeText(context, "Successfully saved Image", Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            } else {
                Log.e("SensorSampleActivity", "Error saving bitmap");
                Toast t = Toast.makeText(context, "Something went wrong, try again.", Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
            }
            progress.dismiss();
        }
    }
}
