package fieldscience.cs.earlham.edu.fieldday;

import java.util.ArrayList;

public interface SensorFragmentCommunication {
    void setSensorList(ArrayList<aSensor> sensorList);
    void enableButtons();
}
