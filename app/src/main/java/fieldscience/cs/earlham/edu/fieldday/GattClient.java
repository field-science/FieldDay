package fieldscience.cs.earlham.edu.fieldday;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.UUID;

class GattClient {
    private BluetoothGatt gattClient;
    private boolean connected;
    private Listener mListener;

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                connected = true;
                gattClient.discoverServices();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                connected = false;
            }
        }
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                mListener.onServicesDiscovered();
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                mListener.onMessageReceived(characteristic.getValue(), characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            mListener.onMessageReceived(characteristic.getValue(), characteristic);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
            Log.d("Characteristic Write", "wrote characteristic" + characteristic.getUuid().toString());
        }
    };

    public void connect(Context context, BluetoothDevice device){
        if (gattClient != null){
            gattClient.disconnect();
            gattClient.close();
        }
        connected = false;
        gattClient = device.connectGatt(context, false, mGattCallback);
    }

    public void isConnected(){
        if (gattClient != null) {
            gattClient.connect();
        }
    }

    public void disconnect(){
        if (gattClient != null){
            gattClient.disconnect();
            gattClient.close();
        }
        gattClient = null;
    }

    public List<BluetoothGattService> getServices(){
        return gattClient.getServices();
    }

    public BluetoothGattService getService(UUID uuid) {
        return gattClient.getService(uuid);
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        gattClient.writeCharacteristic(characteristic);
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic){
        gattClient.readCharacteristic(characteristic);
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        gattClient.setCharacteristicNotification(characteristic, enabled);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID
                .fromString(GattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        gattClient.writeDescriptor(descriptor);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public interface Listener {
        void onMessageReceived(byte[] data, BluetoothGattCharacteristic characteristic);
        void onServicesDiscovered();
    }

}
