package fieldscience.cs.earlham.edu.fieldday;

import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import androidx.preference.CheckBoxPreference;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.Preference.OnPreferenceChangeListener;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import java.util.Objects;

public class SettingsFragment extends PreferenceFragmentCompat {
        static String shortDeviceID = (Build.SERIAL).substring((Build.SERIAL).length()-4);
        private CheckBoxPreference writeWithoutGPS;

        public int info = BuildConfig.VERSION_CODE;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);

            writeWithoutGPS = findPreference("Only write file if GPS signal is present");
            Objects.requireNonNull(writeWithoutGPS).setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SharedPreferences prefs = getSettings(getActivity());
                    Editor editor = prefs.edit();
                    if (writeWithoutGPS.isChecked()) {
                        editor.putBoolean("Only_write_with_GPS", true);
                    }
                    else {
                        editor.putBoolean("Only_write_with_GPS", false);
                    }
                    return editor.commit();
                }
            });

            EditTextPreference transfer = findPreference(getString(R.string.transfer_url_title));
            Objects.requireNonNull(transfer).setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String url = (String) newValue;
                    String scriptURL = "cgi-bin/post_receive.cgi";
                    if (url.length() > 7
                            && (! "http://".equals(url.substring(0, 6))
                            || ! "https://".equals(url.substring(0, 7))))
                        url = "http://" + url;
                    if (url.substring(url.length() - 1).equals("/"))
                        url = url + scriptURL;
                    else
                        url = url + "/" + scriptURL;

                    url.replaceAll("\\s","");
                    SharedPreferences prefs = getSettings(getActivity());
                    Editor editor = prefs.edit();
                    editor.putString("transfer_url", url);
                    return editor.commit();
                }
            });


            //Find and display current info.
            Preference deviceModel = findPreference(getString(R.string.device_model));
            Objects.requireNonNull(deviceModel).setSummary(Build.MODEL);

            Preference deviceID = findPreference(getString(R.string.device_id));
            Objects.requireNonNull(deviceID).setSummary(Build.SERIAL);

            Preference buildInfo = findPreference(getString(R.string.build_info));
            Objects.requireNonNull(buildInfo).setSummary(BuildConfig.VERSION_NAME);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        }

        private static SharedPreferences getSettings(final ContextWrapper context) {
            return PreferenceManager.getDefaultSharedPreferences(context);
        }
}