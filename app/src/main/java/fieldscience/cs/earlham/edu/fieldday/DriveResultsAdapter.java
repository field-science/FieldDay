package fieldscience.cs.earlham.edu.fieldday;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.widget.DataBufferAdapter;

class DriveResultsAdapter extends DataBufferAdapter<Metadata> {

    private final Context context;

    public DriveResultsAdapter(Context context) {
        super(context, R.layout.file_list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(getContext(),
                    R.layout.file_list, null);
        }
        Metadata metadata = getItem(position);
        TextView titleTextView =
                convertView.findViewById(R.id.objectName);
        ImageView objectType = convertView.findViewById(R.id.objectType);
        Resources res = context.getResources();
        switch(metadata.getMimeType()){
            case DriveFolder.MIME_TYPE:
                objectType.setImageDrawable(res.getDrawable(R.drawable.folder_white_icon, null));
                break;
            case "text/html":
            case "text/plain":
                objectType.setImageDrawable(res.getDrawable(R.drawable.file_white_icon,null));
                break;
        }
        titleTextView.setText(metadata.getTitle());
        return convertView;
    }
}